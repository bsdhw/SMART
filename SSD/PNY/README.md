PNY Solid State Drives
======================

This is a list of all tested PNY solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| PNY       | CS900 120GB SSD    | 120 GB | 4       | 189   | 0     | 0.52   |
| PNY       | SSD2SC240G5LC70... | 240 GB | 1       | 13    | 0     | 0.04   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| PNY       | Phison Driven SSDs     | 1      | 4       | 189   | 0     | 0.52   |
| PNY       | Unknown                | 1      | 1       | 13    | 0     | 0.04   |
