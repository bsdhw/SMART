SPCC Solid State Drives
=======================

This is a list of all tested SPCC solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| SPCC      | SSD                | 1 TB   | 1       | 617   | 0     | 1.69   |
| SPCC      | SSD                | 64 GB  | 2       | 424   | 0     | 1.16   |
| SPCC      | SSD                | 120 GB | 1       | 228   | 0     | 0.63   |
| SPCC      | SSD                | 256 GB | 1       | 71    | 0     | 0.19   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| SPCC      | Unknown                | 1      | 1       | 617   | 0     | 1.69   |
| SPCC      | Phison Driven OEM SSDs | 3      | 4       | 287   | 0     | 0.79   |
