OCZ Solid State Drives
======================

This is a list of all tested OCZ solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| OCZ       | AGILITY3           | 120 GB | 1       | 2119  | 0     | 5.81   |
| OCZ       | VERTEX3            | 64 GB  | 2       | 1948  | 0     | 5.34   |
| OCZ       | VERTEX3            | 120 GB | 1       | 1657  | 0     | 4.54   |
| OCZ       | VERTEX2            | 120 GB | 1       | 835   | 0     | 2.29   |
| OCZ       | VERTEX4            | 128 GB | 1       | 1188  | 3     | 0.81   |
| OCZ       | VERTEX4            | 64 GB  | 1       | 206   | 0     | 0.57   |
| OCZ       | AGILITY4           | 128 GB | 1       | 176   | 1     | 0.24   |
| OCZ       | TRION150           | 240 GB | 1       | 40    | 0     | 0.11   |
| OCZ       | VERTEX-TURBO       | 32 GB  | 2       | 298   | 56    | 0.09   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| OCZ       | SandForce Driven SSDs  | 4      | 5       | 1701  | 0     | 4.66   |
| OCZ       | Indilinx Barefoot_2... | 3      | 3       | 523   | 2     | 0.54   |
| OCZ       | OCZ/Toshiba Trion SSDs | 1      | 1       | 40    | 0     | 0.11   |
| OCZ       | Indilinx Barefoot b... | 1      | 2       | 298   | 56    | 0.09   |
