Crucial Solid State Drives
==========================

This is a list of all tested Crucial solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Crucial   | FCCT128M4SSD1      | 128 GB | 2       | 1042  | 0     | 2.86   |
| Crucial   | CT250MX200SSD1     | 250 GB | 5       | 1048  | 1     | 2.68   |
| Crucial   | CT240M500SSD3      | 240 GB | 1       | 917   | 0     | 2.51   |
| Crucial   | CT1050MX300SSD1    | 1 TB   | 3       | 804   | 0     | 2.20   |
| Crucial   | M4-CT064M4SSD2     | 64 GB  | 1       | 589   | 0     | 1.61   |
| Crucial   | CT525MX300SSD1     | 528 GB | 5       | 512   | 1     | 1.08   |
| Crucial   | CT120M500SSD1      | 120 GB | 2       | 369   | 0     | 1.01   |
| Crucial   | CT275MX300SSD4     | 275 GB | 1       | 213   | 0     | 0.59   |
| Crucial   | CT250MX500SSD1     | 250 GB | 7       | 203   | 0     | 0.56   |
| Crucial   | CT500MX200SSD1     | 500 GB | 2       | 199   | 0     | 0.55   |
| Crucial   | CT120BX300SSD1     | 120 GB | 1       | 108   | 0     | 0.30   |
| Crucial   | CT1000MX500SSD1    | 1 TB   | 3       | 89    | 0     | 0.24   |
| Crucial   | CT500MX500SSD1     | 500 GB | 1       | 77    | 0     | 0.21   |
| Crucial   | CT250MX500SSD4     | 250 GB | 1       | 47    | 0     | 0.13   |
| Crucial   | CT120BX500SSD1     | 120 GB | 2       | 39    | 0     | 0.11   |
| Crucial   | CT480M500SSD1      | 480 GB | 1       | 580   | 20    | 0.08   |
| Crucial   | CT240BX500SSD1     | 240 GB | 2       | 23    | 0     | 0.06   |
| Crucial   | CT2000MX500SSD1    | 2 TB   | 1       | 10    | 0     | 0.03   |
| Crucial   | M4-CT128M4SSD3     | 128 GB | 1       | 4     | 0     | 0.01   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Crucial   | Unknown                | 1      | 2       | 1042  | 0     | 2.86   |
| Crucial   | BX/MX1/2/3/500, M5/... | 9      | 23      | 530   | 1     | 1.34   |
| Crucial   | RealSSD m4/C400/P400   | 2      | 2       | 296   | 0     | 0.81   |
| Crucial   | BX/MX1/2/3/500, M5/... | 3      | 5       | 273   | 4     | 0.45   |
| Crucial   | MX500 SSDs             | 4      | 10      | 155   | 0     | 0.43   |
