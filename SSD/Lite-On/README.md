Lite-On Solid State Drives
==========================

This is a list of all tested Lite-On solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Lite-On   | CV8-8E256-11 SATA  | 256 GB | 1       | 143   | 0     | 0.39   |
| Lite-On   | LCS-128M6S-HP      | 128 GB | 1       | 123   | 0     | 0.34   |
| Lite-On   | LCH-256V2S         | 256 GB | 1       | 12    | 0     | 0.03   |
| Lite-On   | LCH-128V2S         | 128 GB | 1       | 1     | 0     | 0.00   |
