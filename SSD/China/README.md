China Solid State Drives
========================

This is a list of all tested China solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| China     | SATA SSD           | 120 GB | 1       | 933   | 0     | 2.56   |
| China     | SSD 128G           | 128 GB | 1       | 181   | 0     | 0.50   |
| China     | 40GB SATA Flash... | 40 GB  | 1       | 156   | 0     | 0.43   |
| China     | OSSD256GBTSS2      | 256 GB | 1       | 107   | 0     | 0.30   |
| China     | BR                 | 64 GB  | 1       | 106   | 0     | 0.29   |
| China     | CS2246-M512        | 506 GB | 1       | 22    | 0     | 0.06   |
| China     | SATA SSD           | 256 GB | 1       | 21    | 0     | 0.06   |
| China     | MSATA 64GB SSD     | 64 GB  | 1       | 1     | 0     | 0.00   |
