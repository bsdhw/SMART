Transcend Solid State Drives
============================

This is a list of all tested Transcend solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Transcend | TS64GSSD25S-M      | 64 GB  | 1       | 1067  | 0     | 2.92   |
| Transcend | TS128GSSD370S      | 128 GB | 1       | 868   | 0     | 2.38   |
| Transcend | TS512GSSD370       | 512 GB | 1       | 412   | 0     | 1.13   |
| Transcend | TS256GSSD230S      | 256 GB | 1       | 147   | 0     | 0.40   |
| Transcend | TS240GMTS420S      | 240 GB | 2       | 77    | 0     | 0.21   |
| Transcend | TS64GMSA370        | 64 GB  | 2       | 47    | 0     | 0.13   |
| Transcend | TS32GMSA370        | 32 GB  | 3       | 35    | 0     | 0.10   |
| Transcend | TS512GMSA370       | 512 GB | 1       | 21    | 0     | 0.06   |
| Transcend | TS256GMTS400       | 256 GB | 1       | 14    | 0     | 0.04   |
| Transcend | TS128GMSA370       | 128 GB | 1       | 11    | 0     | 0.03   |
| Transcend | TS128GMTS430S      | 128 GB | 1       | 8     | 0     | 0.02   |
| Transcend | TS120GMTS420S      | 120 GB | 2       | 2     | 0     | 0.01   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Transcend | JMicron based SSDs     | 1      | 1       | 1067  | 0     | 2.92   |
| Transcend | Silicon Motion base... | 11     | 16      | 115   | 0     | 0.32   |
