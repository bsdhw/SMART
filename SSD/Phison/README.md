Phison Solid State Drives
=========================

This is a list of all tested Phison solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Phison    | SATA SSD           | 16 GB  | 9       | 533   | 0     | 1.46   |
| Phison    | SATA SSD           | 128 GB | 1       | 316   | 0     | 0.87   |
| Phison    | SATA SSD           | 120 GB | 1       | 273   | 0     | 0.75   |
| Phison    | SATA SSD           | 32 GB  | 1       | 145   | 0     | 0.40   |
| Phison    | SATA SSD           | 64 GB  | 1       | 33    | 0     | 0.09   |
