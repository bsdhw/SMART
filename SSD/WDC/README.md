WDC Solid State Drives
======================

This is a list of all tested WDC solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| WDC       | WDBNCE5000PNC      | 500 GB | 1       | 2730  | 0     | 7.48   |
| WDC       | WDS120G1G0A-00SS50 | 120 GB | 1       | 1219  | 0     | 3.34   |
| WDC       | WDS250G1B0A-00H9H0 | 250 GB | 1       | 651   | 0     | 1.78   |
| WDC       | WDS500G2B0A-00SM50 | 500 GB | 1       | 223   | 0     | 0.61   |
| WDC       | WDS500G1B0A-00H9H0 | 500 GB | 1       | 196   | 0     | 0.54   |
| WDC       | WDS240G2G0B-00EPW0 | 240 GB | 2       | 168   | 0     | 0.46   |
| WDC       | WDS500G2B0A        | 500 GB | 2       | 125   | 0     | 0.34   |
| WDC       | WDS120G2G0A-00JH30 | 120 GB | 1       | 82    | 0     | 0.23   |
| WDC       | WDS480G2G0A-00JH30 | 480 GB | 3       | 60    | 0     | 0.16   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 8       | 45    | 1     | 0.11   |
| WDC       | WDS120G1G0B-00RC30 | 120 GB | 1       | 37    | 0     | 0.10   |
| WDC       | WDS120G2G0B-00EPW0 | 120 GB | 1       | 26    | 0     | 0.07   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 26      | 8     | 0     | 0.02   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| WDC       | Blue / Red / Green ... | 2      | 3       | 1022  | 0     | 2.80   |
| WDC       | Blue and Green SSDs    | 11     | 46      | 75    | 1     | 0.20   |
