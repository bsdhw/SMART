SanDisk Solid State Drives
==========================

This is a list of all tested SanDisk solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| SanDisk   | SSD U100           | 16 GB  | 1       | 1440  | 0     | 3.95   |
| SanDisk   | Ultra II           | 960 GB | 2       | 1202  | 0     | 3.29   |
| SanDisk   | SD7SF6S512G1122    | 512 GB | 1       | 1073  | 0     | 2.94   |
| SanDisk   | SD6SB1M064G1022I   | 64 GB  | 2       | 898   | 0     | 2.46   |
| SanDisk   | SD8SN8U-256G-1006  | 256 GB | 1       | 888   | 0     | 2.44   |
| SanDisk   | SD8SN8U128G1122    | 128 GB | 2       | 848   | 0     | 2.33   |
| SanDisk   | SSD U110           | 16 GB  | 3       | 797   | 0     | 2.19   |
| SanDisk   | Ultra II           | 480 GB | 2       | 622   | 0     | 1.70   |
| SanDisk   | SDSSDHII120G       | 120 GB | 1       | 613   | 0     | 1.68   |
| SanDisk   | SDSSDXP120G        | 120 GB | 1       | 608   | 0     | 1.67   |
| SanDisk   | SDSSDA120G         | 120 GB | 2       | 566   | 0     | 1.55   |
| SanDisk   | SD8SN8U128G1002    | 128 GB | 1       | 424   | 0     | 1.16   |
| SanDisk   | SDSSDH3512G        | 512 GB | 1       | 415   | 0     | 1.14   |
| SanDisk   | SSD PLUS           | 120 GB | 8       | 312   | 0     | 0.86   |
| SanDisk   | SD9SN8W128G1002    | 128 GB | 1       | 278   | 0     | 0.76   |
| SanDisk   | SD6SB2M512G1022I   | 512 GB | 1       | 536   | 1     | 0.74   |
| SanDisk   | SD5SG2256G1052E    | 256 GB | 1       | 246   | 0     | 0.68   |
| SanDisk   | X400 M.2 2280      | 512 GB | 1       | 204   | 0     | 0.56   |
| SanDisk   | SD6SB1M128G1022I   | 128 GB | 1       | 139   | 0     | 0.38   |
| SanDisk   | SD7SN3Q128G1002    | 128 GB | 1       | 131   | 0     | 0.36   |
| SanDisk   | SDSSDH3 512G       | 512 GB | 1       | 116   | 0     | 0.32   |
| SanDisk   | SD9SN8W128G1102    | 128 GB | 1       | 98    | 0     | 0.27   |
| SanDisk   | Ultra II           | 240 GB | 1       | 58    | 0     | 0.16   |
| SanDisk   | SD6SB1M256G1022I   | 256 GB | 2       | 1754  | 44    | 0.11   |
| SanDisk   | SSD PLUS           | 1 TB   | 1       | 37    | 0     | 0.10   |
| SanDisk   | SSD PLUS           | 240 GB | 2       | 43    | 1     | 0.06   |
| SanDisk   | SSD i100           | 64 GB  | 1       | 21    | 0     | 0.06   |
| SanDisk   | SDSSDH3 250G       | 250 GB | 1       | 0     | 0     | 0.00   |
| SanDisk   | SD7UB3Q256G1001    | 256 GB | 1       | 84    | 100   | 0.00   |
| SanDisk   | SSD G5 BICS4       | 500 GB | 1       | 0     | 0     | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| SanDisk   | SanDisk based SSDs     | 4      | 6       | 656   | 17    | 1.76   |
| SanDisk   | SandForce Driven SSDs  | 1      | 2       | 566   | 0     | 1.55   |
| SanDisk   | Marvell based SanDi... | 15     | 28      | 584   | 4     | 1.24   |
| SanDisk   | Unknown                | 10     | 10      | 274   | 0     | 0.75   |
