Goodram Solid State Drives
==========================

This is a list of all tested Goodram solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Goodram   | SSD                | 240 GB | 2       | 1509  | 0     | 4.13   |
| Goodram   | SSD                | 120 GB | 1       | 172   | 0     | 0.47   |
