Hoodisk Solid State Drives
==========================

This is a list of all tested Hoodisk solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Hoodisk   | SSD                | 32 GB  | 3       | 372   | 0     | 1.02   |
| Hoodisk   | SSD                | 64 GB  | 2       | 180   | 0     | 0.50   |
