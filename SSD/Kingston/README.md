Kingston Solid State Drives
===========================

This is a list of all tested Kingston solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Kingston  | SH103S3240G        | 240 GB | 1       | 1798  | 0     | 4.93   |
| Kingston  | SMS200S360G        | 64 GB  | 2       | 1773  | 0     | 4.86   |
| Kingston  | SKC400S37512G      | 512 GB | 2       | 1059  | 0     | 2.90   |
| Kingston  | SH103S3120G        | 120 GB | 2       | 964   | 0     | 2.64   |
| Kingston  | SUV400S37240G      | 240 GB | 3       | 815   | 0     | 2.23   |
| Kingston  | SUV300S37A240G     | 240 GB | 1       | 807   | 0     | 2.21   |
| Kingston  | SMSM150S324G2      | 24 GB  | 1       | 650   | 0     | 1.78   |
| Kingston  | SUV500MS120G       | 120 GB | 3       | 611   | 0     | 1.68   |
| Kingston  | SV300S37A480G      | 480 GB | 1       | 501   | 0     | 1.37   |
| Kingston  | SVP200S37A60G      | 64 GB  | 1       | 377   | 0     | 1.03   |
| Kingston  | RBUSNS8180S3128GI  | 128 GB | 1       | 351   | 0     | 0.96   |
| Kingston  | SHFS37A120G        | 120 GB | 1       | 334   | 0     | 0.92   |
| Kingston  | SA400S37240G       | 240 GB | 12      | 308   | 0     | 0.85   |
| Kingston  | SUV500MS240G       | 240 GB | 4       | 307   | 0     | 0.84   |
| Kingston  | SV300S37A120G      | 120 GB | 4       | 410   | 1     | 0.77   |
| Kingston  | SMS200S3120G       | 120 GB | 3       | 881   | 3     | 0.76   |
| Kingston  | SA400S37120G       | 120 GB | 7       | 271   | 0     | 0.74   |
| Kingston  | SMS200S330G        | 32 GB  | 2       | 768   | 2     | 0.60   |
| Kingston  | SUV500240G         | 240 GB | 2       | 198   | 0     | 0.55   |
| Kingston  | SM2280S3G2240G     | 240 GB | 1       | 181   | 0     | 0.50   |
| Kingston  | SA400S37480G       | 480 GB | 3       | 119   | 0     | 0.33   |
| Kingston  | SMSM151S3128GD     | 128 GB | 1       | 598   | 7     | 0.21   |
| Kingston  | SUV500MS480G       | 480 GB | 2       | 49    | 0     | 0.14   |
| Kingston  | SUV400S37 120G     | 120 GB | 1       | 37    | 0     | 0.10   |
| Kingston  | SHSS37A240G        | 240 GB | 1       | 25    | 0     | 0.07   |
| Kingston  | SHFS37A240G        | 240 GB | 1       | 4     | 0     | 0.01   |
| Kingston  | SV300S37A60G       | 64 GB  | 1       | 1586  | 804   | 0.01   |
| Kingston  | SNS4151S316G       | 16 GB  | 1       | 1252  | 1022  | 0.00   |
| Kingston  | RBU-SNS4151S316GG2 | 16 GB  | 1       | 0     | 0     | 0.00   |
| Kingston  | SNS4151S332G       | 32 GB  | 1       | 79    | 1022  | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Kingston  | SandForce Driven SSDs  | 11     | 19      | 836   | 44    | 1.57   |
| Kingston  | SSDNow UV400/500       | 5      | 14      | 429   | 0     | 1.18   |
| Kingston  | Phison Driven SSDs     | 6      | 26      | 342   | 0     | 0.94   |
| Kingston  | Unknown                | 8      | 8       | 393   | 257   | 0.44   |
