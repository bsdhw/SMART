Apacer Solid State Drives
=========================

This is a list of all tested Apacer solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Apacer    | 128GB SATA Flas... | 128 GB | 1       | 187   | 0     | 0.51   |
| Apacer    | 32GB SATA Flash... | 32 GB  | 1       | 1769  | 10    | 0.44   |
| Apacer    | AS350              | 256 GB | 1       | 30    | 0     | 0.08   |
| Apacer    | AS350              | 128 GB | 1       | 4     | 0     | 0.01   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Apacer    | SDM5/5A/5A-M Series... | 1      | 1       | 1769  | 10    | 0.44   |
| Apacer    | Unknown                | 3      | 3       | 74    | 0     | 0.20   |
