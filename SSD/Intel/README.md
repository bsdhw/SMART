Intel Solid State Drives
========================

This is a list of all tested Intel solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Intel     | SSDSA2M040G2GC     | 40 GB  | 1       | 2739  | 0     | 7.51   |
| Intel     | SSDSC2CT180A3      | 180 GB | 2       | 2012  | 0     | 5.51   |
| Intel     | SSDSC2CT080A4      | 80 GB  | 1       | 1964  | 0     | 5.38   |
| Intel     | SSDSC2BF120A5      | 120 GB | 1       | 1960  | 0     | 5.37   |
| Intel     | SSDSA2CT040G3      | 40 GB  | 2       | 1958  | 0     | 5.37   |
| Intel     | SSDSC2BW240A3L     | 240 GB | 1       | 1425  | 0     | 3.91   |
| Intel     | SSDSC2BB080G4      | 80 GB  | 1       | 1060  | 0     | 2.91   |
| Intel     | SSDSC2BB016T4      | 1.6 TB | 1       | 959   | 0     | 2.63   |
| Intel     | SSDSC2BP240G4      | 240 GB | 1       | 835   | 0     | 2.29   |
| Intel     | SSDSC2BW240H6      | 240 GB | 2       | 723   | 0     | 1.98   |
| Intel     | SSDSA2CW160G3      | 160 GB | 1       | 610   | 0     | 1.67   |
| Intel     | SSDSC2BB120G4      | 120 GB | 1       | 413   | 0     | 1.13   |
| Intel     | SSDSC2BB480G7      | 480 GB | 1       | 703   | 1     | 0.96   |
| Intel     | SSDSA2M080G2GC     | 80 GB  | 1       | 3057  | 8     | 0.93   |
| Intel     | SSDSA2CW120G3      | 120 GB | 1       | 326   | 0     | 0.89   |
| Intel     | SSDSC2CT240A3      | 240 GB | 1       | 2370  | 7     | 0.81   |
| Intel     | SSDSC2BF180A4L     | 180 GB | 3       | 392   | 2     | 0.72   |
| Intel     | SSDSA2M160G2GC     | 160 GB | 1       | 472   | 1     | 0.65   |
| Intel     | SSDSCKGF180A4L     | 180 GB | 1       | 152   | 0     | 0.42   |
| Intel     | SSDSC2CW060A3      | 64 GB  | 2       | 1461  | 509   | 0.38   |
| Intel     | SSDSC2KG480G8R     | 480 GB | 2       | 105   | 0     | 0.29   |
| Intel     | SSDSC2BW240A4      | 240 GB | 1       | 89    | 0     | 0.24   |
| Intel     | SSDSC2KG480G8      | 480 GB | 3       | 70    | 0     | 0.19   |
| Intel     | SSDSC2BF180A5H REF | 180 GB | 1       | 13    | 0     | 0.04   |
| Intel     | SSDSC2BA200G3T     | 200 GB | 2       | 1873  | 1028  | 0.00   |
| Intel     | SSDSCKKF256G8H     | 256 GB | 1       | 144   | 89    | 0.00   |
| Intel     | SSDSC2CW120A3      | 120 GB | 1       | 1235  | 1019  | 0.00   |
| Intel     | SSDSC2KW120H6      | 120 GB | 1       | 2     | 29    | 0.00   |
| Intel     | SSDSC2KW480H6      | 480 GB | 1       | 39    | 1010  | 0.00   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Intel     | 330/335 Series SSDs    | 2      | 3       | 2131  | 3     | 3.95   |
| Intel     | 320 Series SSDs        | 3      | 4       | 1213  | 0     | 3.32   |
| Intel     | X18-M/X25-M/X25-V G... | 3      | 3       | 2090  | 3     | 3.03   |
| Intel     | 53x and Pro 1500/25... | 3      | 4       | 874   | 0     | 2.39   |
| Intel     | 730 and DC S35x0/36... | 6      | 7       | 1102  | 294   | 1.42   |
| Intel     | 520 Series SSDs        | 3      | 4       | 1395  | 509   | 1.17   |
| Intel     | Unknown                | 5      | 7       | 493   | 14    | 1.14   |
| Intel     | Dell Certified Inte... | 1      | 2       | 105   | 0     | 0.29   |
| Intel     | S4510/S4610/S4500/S... | 1      | 3       | 70    | 0     | 0.19   |
| Intel     | 540 Series SSDs        | 2      | 2       | 20    | 520   | 0.00   |
