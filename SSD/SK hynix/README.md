SK hynix Solid State Drives
===========================

This is a list of all tested SK hynix solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| SK hynix  | HFS256G3AMNB-2200A | 256 GB | 1       | 1542  | 0     | 4.22   |
| SK hynix  | HFS128G32MND-2200A | 128 GB | 1       | 798   | 2     | 0.73   |
| SK hynix  | HFS128G39TND-N210A | 128 GB | 1       | 232   | 0     | 0.64   |
| SK hynix  | SC311 SATA         | 512 GB | 1       | 153   | 0     | 0.42   |
| SK hynix  | SC210 mSATA        | 256 GB | 1       | 391   | 139   | 0.01   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| SK hynix  | Unknown                | 1      | 1       | 1542  | 0     | 4.22   |
| SK hynix  | SATA SSDs              | 4      | 4       | 393   | 36    | 0.45   |
