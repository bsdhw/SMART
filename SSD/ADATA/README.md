ADATA Solid State Drives
========================

This is a list of all tested ADATA solid state drive models and their MTBFs. See
more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ SSD by Model  ](#ssd-by-model)
2. [ SSD by Family ](#ssd-by-family)

SSD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| ADATA     | SP310              | 32 GB  | 1       | 1944  | 0     | 5.33   |
| ADATA     | SP900              | 128 GB | 1       | 1548  | 0     | 4.24   |
| ADATA     | SP310              | 128 GB | 1       | 1488  | 0     | 4.08   |
| ADATA     | SX930              | 240 GB | 1       | 1001  | 0     | 2.74   |
| ADATA     | SU800              | 256 GB | 1       | 85    | 0     | 0.24   |
| ADATA     | SP600              | 64 GB  | 1       | 84    | 0     | 0.23   |
| ADATA     | SU800              | 1 TB   | 1       | 83    | 0     | 0.23   |
| ADATA     | SU630              | 480 GB | 1       | 48    | 0     | 0.13   |
| ADATA     | SU750              | 256 GB | 1       | 36    | 0     | 0.10   |
| ADATA     | SU650              | 120 GB | 2       | 406   | 106   | 0.05   |

SSD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| ADATA     | SandForce Driven SSDs  | 1      | 1       | 1548  | 0     | 4.24   |
| ADATA     | JMicron based SSDs     | 4      | 4       | 1129  | 0     | 3.09   |
| ADATA     | Silicon Motion base... | 2      | 2       | 84    | 0     | 0.23   |
| ADATA     | Unknown                | 3      | 4       | 224   | 53    | 0.08   |
