Appendix 6: Top 1000 NVMe Models
================================

See more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Samsung   | SSD 950 PRO        | 512 GB | 2       | 1098  | 0     | 3.01   |
| Samsung   | MZVPV256HDGL-000L7 | 256 GB | 1       | 915   | 0     | 2.51   |
| Samsung   | MZVLW256HEHP-000L7 | 256 GB | 1       | 637   | 0     | 1.75   |
| Samsung   | SSD 960 EVO        | 250 GB | 3       | 612   | 0     | 1.68   |
| Plextor   | PX-256M8PeG        | 256 GB | 1       | 598   | 0     | 1.64   |
| HP        | SSD EX900          | 120 GB | 1       | 591   | 0     | 1.62   |
| HP        | SSD EX920          | 512 GB | 1       | 531   | 0     | 1.46   |
| Patriot   | Scorch M2          | 128 GB | 1       | 493   | 0     | 1.35   |
| Samsung   | SSD 970 PRO        | 512 GB | 1       | 466   | 0     | 1.28   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 1       | 456   | 0     | 1.25   |
| Intel     | SSDPEKNW010T8      | 1 TB   | 1       | 405   | 0     | 1.11   |
| Intel     | SSDPEKKF256G8L     | 256 GB | 1       | 376   | 0     | 1.03   |
| Toshiba   | KXG50ZNV512G       | 512 GB | 1       | 363   | 0     | 1.00   |
| Corsair   | Force MP600        | 2 TB   | 1       | 308   | 0     | 0.85   |
| Intel     | SSDPED1D480GA      | 480 GB | 1       | 299   | 0     | 0.82   |
| SPCC      | M.2 PCIe SSD       | 512 GB | 2       | 297   | 0     | 0.81   |
| Samsung   | SSD 960 EVO        | 500 GB | 1       | 264   | 0     | 0.72   |
| WDC       | WDS500G1B0C-00S6U0 | 500 GB | 1       | 257   | 0     | 0.71   |
| Toshiba   | THNSF5256GPUK      | 256 GB | 1       | 247   | 0     | 0.68   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | 1       | 239   | 0     | 0.66   |
| ADATA     | SX8200PNP          | 1 TB   | 2       | 228   | 0     | 0.63   |
| Samsung   | MZVLB512HAJQ-000L7 | 512 GB | 4       | 252   | 1     | 0.53   |
| Intel     | SSDPEDMW800G4      | 800 GB | 1       | 168   | 0     | 0.46   |
| Toshiba   | RC100              | 240 GB | 1       | 151   | 0     | 0.41   |
| ORICO     | V500               | 1 TB   | 1       | 149   | 0     | 0.41   |
| WDC       | WDS250G2B0C-00PXH0 | 250 GB | 1       | 137   | 0     | 0.38   |
| Toshiba   | THNSF5256GCJ7      | 256 GB | 1       | 130   | 0     | 0.36   |
| Samsung   | PM961 NVMe         | 512 GB | 1       | 116   | 0     | 0.32   |
| Crucial   | CT1000P1SSD8       | 1 TB   | 5       | 111   | 0     | 0.30   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | 1       | 109   | 0     | 0.30   |
| Corsair   | Force MP300        | 120 GB | 1       | 102   | 0     | 0.28   |
| Corsair   | Force MP600        | 500 GB | 1       | 91    | 0     | 0.25   |
| SPCC      | M.2 PCIe SSD       | 1 TB   | 2       | 89    | 0     | 0.24   |
| Samsung   | SSD 970 PRO        | 1 TB   | 2       | 83    | 0     | 0.23   |
| Intel     | HBRPEKNX0202AHO    | 32 GB  | 1       | 81    | 0     | 0.22   |
| Intel     | HBRPEKNX0202AH     | 512 GB | 1       | 80    | 0     | 0.22   |
| Samsung   | MZVLB512HBJQ-000L7 | 512 GB | 2       | 79    | 0     | 0.22   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 5       | 67    | 0     | 0.19   |
| SK hynix  | BC501 HFM256GDJ... | 256 GB | 2       | 60    | 0     | 0.17   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 2       | 51    | 0     | 0.14   |
| Intel     | SSDPEKNW512G8      | 512 GB | 1       | 46    | 0     | 0.13   |
| Samsung   | SSD 970 EVO        | 500 GB | 1       | 42    | 0     | 0.12   |
| Samsung   | PM981 NVMe         | 256 GB | 1       | 40    | 0     | 0.11   |
| Samsung   | SSD 960 EVO        | 1 TB   | 1       | 154   | 3     | 0.11   |
| Phison    | Sabrent Rocket 4.0 | 1 TB   | 1       | 36    | 0     | 0.10   |
| WDC       | PC SN730 SDBQNT... | 512 GB | 1       | 32    | 0     | 0.09   |
| WDC       | PC SN730 SDBQNT... | 1 TB   | 1       | 32    | 0     | 0.09   |
| Intel     | SSDPEKKF512G8L     | 512 GB | 2       | 30    | 0     | 0.08   |
| Kingston  | RBUSNS8154P3512GJ  | 512 GB | 1       | 25    | 0     | 0.07   |
| WDC       | PC SN730 SDBQNT... | 256 GB | 1       | 23    | 0     | 0.06   |
| SK hynix  | PC401 NVMe         | 256 GB | 1       | 22    | 0     | 0.06   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 1       | 18    | 0     | 0.05   |
| Samsung   | MZVLB256HBHQ-000L7 | 256 GB | 1       | 17    | 0     | 0.05   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 1       | 13    | 0     | 0.04   |
| Lite-On   | CA3-8D512          | 512 GB | 1       | 10    | 0     | 0.03   |
| Phison    | PCIe SSD           | 512 GB | 1       | 10    | 0     | 0.03   |
| Micron    | 2200V_MTFDHBA51... | 512 GB | 1       | 7     | 0     | 0.02   |
| Union ... | UMIS LENSE40512... | 512 GB | 1       | 6     | 0     | 0.02   |
| Samsung   | MZVLQ256HAJD-00000 | 256 GB | 1       | 5     | 0     | 0.02   |
| Samsung   | MZVLW512HMJP-00000 | 512 GB | 1       | 5     | 0     | 0.02   |
| WDC       | PC SN520 SDAPNU... | 512 GB | 1       | 5     | 0     | 0.01   |
| Toshiba   | KBG30ZMT512G       | 512 GB | 1       | 2     | 0     | 0.01   |
| Union ... | RPFTJ128PDD2EWX    | 128 GB | 1       | 1     | 0     | 0.00   |
| SK hynix  | HFM256GDJTNG-8310A | 256 GB | 1       | 1     | 0     | 0.00   |
| Toshiba   | KXG60PNV2T04 NV... | 2 TB   | 1       | 0     | 0     | 0.00   |
| ADATA     | SX6000LNP          | 512 GB | 1       | 0     | 0     | 0.00   |
| SK hynix  | SKHynix_HFS512G... | 512 GB | 1       | 0     | 0     | 0.00   |
