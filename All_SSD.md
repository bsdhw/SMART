Appendix 2: All SSD Samples
===========================

This is a list of all tested SSD samples and their MTBFs. See more info on
reliability test in the README. See HDD samples MTBFs in the Appendix 1 (All_HDD.md).

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Drive ID     | Days  | Err   | MTBF   |
|-----------|--------------------|--------|--------------|-------|-------|--------|
| Samsung   | SSD 840 EVO        | 120 GB | F115D96E1483 | 4181  | 0     | 11.46  |
| Intel     | SSDSA2CT040G3      | 40 GB  | FFB33E283F18 | 2994  | 0     | 8.20   |
| Samsung   | SSD 850 PRO        | 128 GB | DD04DD677256 | 2742  | 0     | 7.51   |
| Intel     | SSDSA2M040G2GC     | 40 GB  | 778381049770 | 2739  | 0     | 7.51   |
| WDC       | WDBNCE5000PNC      | 500 GB | F2352F56D061 | 2730  | 0     | 7.48   |
| Intel     | SSDSC2CT180A3      | 180 GB | F198E4676CAA | 2673  | 0     | 7.33   |
| Samsung   | SSD 840 EVO        | 120 GB | 4A260925ACF8 | 2673  | 0     | 7.32   |
| Samsung   | SSD 840 Series     | 120 GB | 9E96649CFED6 | 2655  | 0     | 7.28   |
| Samsung   | SSD 840 PRO Series | 128 GB | EAFB5D1B1F26 | 2487  | 0     | 6.81   |
| Samsung   | SSD 840 Series     | 250 GB | 0C1C6E544534 | 2429  | 0     | 6.66   |
| Kingston  | SMS200S360G        | 64 GB  | 71CE82B4D2C6 | 2280  | 0     | 6.25   |
| Samsung   | SSD 840 PRO Series | 256 GB | 36DAD5C5A950 | 2123  | 0     | 5.82   |
| OCZ       | AGILITY3           | 120 GB | 531A2B671C06 | 2119  | 0     | 5.81   |
| Samsung   | MZ7TD256HAFV-000L7 | 256 GB | 0F1341E4655F | 2052  | 0     | 5.62   |
| OCZ       | VERTEX3            | 64 GB  | 7AB979508C45 | 1983  | 0     | 5.43   |
| Intel     | SSDSC2CT080A4      | 80 GB  | 2670BDE5DED6 | 1964  | 0     | 5.38   |
| Intel     | SSDSC2BF120A5      | 120 GB | 9DC9944515D8 | 1960  | 0     | 5.37   |
| ADATA     | SP310              | 32 GB  | A11EDF397DCD | 1944  | 0     | 5.33   |
| OCZ       | VERTEX3            | 64 GB  | 245E71AD4EED | 1914  | 0     | 5.24   |
| Crucial   | CT250MX200SSD1     | 250 GB | CF14859573DD | 1849  | 0     | 5.07   |
| Crucial   | CT250MX200SSD1     | 250 GB | 555F2CD3B385 | 1837  | 0     | 5.04   |
| Samsung   | SSD PB22-JS3 TM    | 64 GB  | 89A98B1FAE33 | 1801  | 0     | 4.94   |
| Kingston  | SH103S3240G        | 240 GB | 3E96D624CB14 | 1798  | 0     | 4.93   |
| Kingston  | SH103S3120G        | 120 GB | 287C4CF44AC6 | 1717  | 0     | 4.71   |
| Samsung   | SSD 850 EVO M.2    | 500 GB | C72976DE50C8 | 1713  | 0     | 4.69   |
| Samsung   | MZMTE512HMHP-000MV | 512 GB | CAA64AA7509A | 1697  | 0     | 4.65   |
| OCZ       | VERTEX3            | 120 GB | 4906AC7AC2F6 | 1657  | 0     | 4.54   |
| Micron    | M600_MTFDDAK256MBF | 256 GB | C69EAB9B1B3C | 1656  | 0     | 4.54   |
| Samsung   | SSD 850 EVO        | 250 GB | 708BDFA58843 | 1648  | 0     | 4.52   |
| Samsung   | SSD 850 EVO        | 250 GB | 8C49516A825E | 1648  | 0     | 4.52   |
| Samsung   | SSD 850 EVO        | 250 GB | E8071C45EA15 | 1643  | 0     | 4.50   |
| SanDisk   | SSD U110           | 16 GB  | BF09A8790181 | 1641  | 0     | 4.50   |
| Samsung   | SSD 850 EVO mSATA  | 250 GB | 3973CE849972 | 1600  | 0     | 4.38   |
| ADATA     | SP900              | 128 GB | A0FCFB4E8962 | 1548  | 0     | 4.24   |
| SK hynix  | HFS256G3AMNB-2200A | 256 GB | E124513FBF75 | 1542  | 0     | 4.22   |
| Goodram   | SSD                | 240 GB | 24AF82D242BD | 1509  | 0     | 4.13   |
| Goodram   | SSD                | 240 GB | 4F3A2E047C7B | 1509  | 0     | 4.13   |
| Samsung   | MZMTE512HMHP-000MV | 512 GB | FE966AF9D066 | 1488  | 0     | 4.08   |
| ADATA     | SP310              | 128 GB | 7B0DBA54C9BD | 1488  | 0     | 4.08   |
| Samsung   | SSD 850 EVO        | 250 GB | 725466A2F3DA | 1485  | 0     | 4.07   |
| SanDisk   | SSD U100           | 16 GB  | 940AF36F847B | 1440  | 0     | 3.95   |
| Intel     | SSDSC2BW240A3L     | 240 GB | 1AFEFC2A89FA | 1425  | 0     | 3.91   |
| Samsung   | SSD 750 EVO        | 120 GB | BEF56A198B40 | 1425  | 0     | 3.91   |
| Kingston  | SUV400S37240G      | 240 GB | C0313DC9C88B | 1409  | 0     | 3.86   |
| SanDisk   | SD6SB1M064G1022I   | 64 GB  | 634BEE3EDBAD | 1399  | 0     | 3.84   |
| Toshiba   | THNSNJ128GCSU      | 128 GB | 096BBE33C093 | 1370  | 0     | 3.75   |
| Samsung   | SSD 850 EVO        | 500 GB | 0D6AF08B8404 | 1364  | 0     | 3.74   |
| Intel     | SSDSC2CT180A3      | 180 GB | 14583978D656 | 1351  | 0     | 3.70   |
| Samsung   | SSD 850 EVO        | 250 GB | 2A18FC6099C0 | 1334  | 0     | 3.66   |
| Samsung   | SSD 850 EVO        | 250 GB | 741AB0880F27 | 1312  | 0     | 3.59   |
| Samsung   | MZ7LM240HCGR-0E003 | 240 GB | 0408D44370D6 | 1295  | 0     | 3.55   |
| Samsung   | MZ7LM240HCGR-0E003 | 240 GB | 80B6DA8D9C6D | 1295  | 0     | 3.55   |
| Crucial   | FCCT128M4SSD1      | 128 GB | 1AD1B7221CE5 | 1267  | 0     | 3.47   |
| Kingston  | SMS200S360G        | 64 GB  | BF8B74F534AB | 1266  | 0     | 3.47   |
| Intel     | SSDSC2BW240H6      | 240 GB | 62B67E8E36C4 | 1258  | 0     | 3.45   |
| Phison    | SATA SSD           | 16 GB  | 0C45208B982B | 1228  | 0     | 3.37   |
| Crucial   | CT525MX300SSD1     | 528 GB | 8DDEE65F1778 | 1226  | 0     | 3.36   |
| WDC       | WDS120G1G0A-00SS50 | 120 GB | 9D0A409F744C | 1219  | 0     | 3.34   |
| Crucial   | CT1050MX300SSD1    | 1 TB   | BE7D100FE99F | 1214  | 0     | 3.33   |
| SanDisk   | Ultra II           | 960 GB | 12459DB064E4 | 1202  | 0     | 3.29   |
| SanDisk   | Ultra II           | 960 GB | 0F240FC307CD | 1202  | 0     | 3.29   |
| Samsung   | SSD 850 EVO        | 1 TB   | F8026E24FE7A | 1199  | 0     | 3.29   |
| Samsung   | SSD RBX Series ... | 64 GB  | 28E948E63BE2 | 1182  | 0     | 3.24   |
| SuperM... | SSD                | 16 GB  | CE8863A4D332 | 1176  | 0     | 3.22   |
| Samsung   | SSD 850 EVO M.2    | 500 GB | 7D4147AF7706 | 1163  | 0     | 3.19   |
| SanDisk   | SDSSDA120G         | 120 GB | 6B6C3D027F5A | 1123  | 0     | 3.08   |
| Samsung   | SSD 840 EVO        | 250 GB | C22F779B83C5 | 1092  | 0     | 2.99   |
| Samsung   | SSD 850 EVO mSATA  | 1 TB   | 43FDDDD82CC6 | 2157  | 1     | 2.96   |
| SanDisk   | SD7SF6S512G1122    | 512 GB | 7553B1015027 | 1073  | 0     | 2.94   |
| Samsung   | SSD 850 PRO        | 256 GB | F1C330086F56 | 1070  | 0     | 2.93   |
| Samsung   | SSD 850 EVO        | 1 TB   | 8BD341C95452 | 1068  | 0     | 2.93   |
| Transcend | TS64GSSD25S-M      | 64 GB  | 7B3E9824D92F | 1067  | 0     | 2.92   |
| Samsung   | MZ7PD128HAFV-000H7 | 128 GB | 86BB9B5BD24A | 1060  | 0     | 2.91   |
| Intel     | SSDSC2BB080G4      | 80 GB  | 595FD41A8ACE | 1060  | 0     | 2.91   |
| Kingston  | SKC400S37512G      | 512 GB | 2D724BD9FF8D | 1059  | 0     | 2.90   |
| Kingston  | SKC400S37512G      | 512 GB | 6D7F99EA4EAF | 1059  | 0     | 2.90   |
| Toshiba   | THNSNJ128GCSU      | 128 GB | 91E68A945423 | 1009  | 0     | 2.77   |
| ADATA     | SX930              | 240 GB | 6333D4BED332 | 1001  | 0     | 2.74   |
| Samsung   | SSD 850 EVO mSATA  | 250 GB | 24525A81E99A | 983   | 0     | 2.69   |
| Phison    | SATA SSD           | 16 GB  | 35B762CF93F6 | 981   | 0     | 2.69   |
| Samsung   | SSD 850 EVO        | 500 GB | F555D3E3DD0B | 969   | 0     | 2.66   |
| Intel     | SSDSC2BB016T4      | 1.6 TB | C920398BFC22 | 959   | 0     | 2.63   |
| China     | SATA SSD           | 120 GB | BD5F60DECB4D | 933   | 0     | 2.56   |
| Samsung   | SSD 850 EVO        | 250 GB | 726CD857B59F | 925   | 0     | 2.53   |
| Samsung   | SSD 850 EVO        | 250 GB | AD49B3EA3046 | 925   | 0     | 2.53   |
| Samsung   | SSD 850 EVO        | 250 GB | DF67D90D2A7A | 925   | 0     | 2.53   |
| Samsung   | SSD 850 EVO        | 250 GB | DF736B6598E9 | 925   | 0     | 2.53   |
| Intel     | SSDSA2CT040G3      | 40 GB  | 8E254778A4F7 | 923   | 0     | 2.53   |
| Crucial   | CT240M500SSD3      | 240 GB | 2883DE3FC7A5 | 917   | 0     | 2.51   |
| SanDisk   | SD8SN8U128G1122    | 128 GB | D84774C43A6F | 911   | 0     | 2.50   |
| Crucial   | CT250MX200SSD1     | 250 GB | 9B6FE8DB248D | 905   | 0     | 2.48   |
| Seagate   | ST100FN0021        | 100 GB | 6881E706597F | 1781  | 1     | 2.44   |
| SanDisk   | SD8SN8U-256G-1006  | 256 GB | 3DC5E385AA2D | 888   | 0     | 2.44   |
| Transcend | TS128GSSD370S      | 128 GB | 9A7DC3267F3B | 868   | 0     | 2.38   |
| Samsung   | SSD 850 PRO        | 256 GB | 8809895F230A | 859   | 0     | 2.35   |
| Samsung   | SSD 850 PRO        | 128 GB | DCA5DF50039B | 854   | 0     | 2.34   |
| Samsung   | SSD 750 EVO        | 500 GB | E986880A1A11 | 854   | 0     | 2.34   |
| Corsair   | Force 3 SSD        | 180 GB | 6343F02E7D11 | 2530  | 2     | 2.31   |
| OCZ       | VERTEX2            | 120 GB | D8F62CE1E9BF | 835   | 0     | 2.29   |
| Intel     | SSDSC2BP240G4      | 240 GB | C3B2B76FA68D | 835   | 0     | 2.29   |
| Kingston  | SUV400S37240G      | 240 GB | 2DC237E36B76 | 833   | 0     | 2.28   |
| Toshiba   | THNSNC128GBSJ      | 128 GB | 51FE645EF10E | 826   | 0     | 2.26   |
| Crucial   | FCCT128M4SSD1      | 128 GB | 17B168CC2ECE | 817   | 0     | 2.24   |
| Crucial   | CT1050MX300SSD1    | 1 TB   | 648ECCD7B1AA | 811   | 0     | 2.22   |
| Kingston  | SUV300S37A240G     | 240 GB | C62D39256F65 | 807   | 0     | 2.21   |
| Phison    | SATA SSD           | 16 GB  | E5E3E2A37C2F | 804   | 0     | 2.20   |
| Samsung   | SSD 860 EVO        | 500 GB | 5FA7624AC57B | 802   | 0     | 2.20   |
| Micron    | 1100_MTFDDAK512TBN | 512 GB | 8F8C02B7B158 | 798   | 0     | 2.19   |
| Micron    | 1100_MTFDDAK512TBN | 512 GB | 891C76D898A2 | 798   | 0     | 2.19   |
| Samsung   | SSD 860 EVO        | 500 GB | E894D8939CA8 | 786   | 0     | 2.16   |
| SanDisk   | SD8SN8U128G1122    | 128 GB | 110FE2B59E67 | 786   | 0     | 2.16   |
| Micron    | 5100_MTFDDAK240TCB | 240 GB | 35184446C3DB | 779   | 0     | 2.14   |
| Micron    | 5100_MTFDDAK240TCB | 240 GB | E194B45FC791 | 779   | 0     | 2.14   |
| Micron    | 5100_MTFDDAK240TCB | 240 GB | 4BE166349018 | 778   | 0     | 2.13   |
| Micron    | 5100_MTFDDAK240TCB | 240 GB | 8ECD1F51A5B7 | 778   | 0     | 2.13   |
| Kingston  | SUV500MS120G       | 120 GB | CBEBAAD8A327 | 773   | 0     | 2.12   |
| MyDigi... | SB2                | 128 GB | 7619BA39920A | 763   | 0     | 2.09   |
| Samsung   | SSD 860 EVO        | 2 TB   | A964BB9D67B6 | 760   | 0     | 2.08   |
| Apple     | SSD TS256C         | 256 GB | 751D09466F92 | 752   | 0     | 2.06   |
| Samsung   | SSD 850 EVO        | 2 TB   | BA86C8D15CEA | 742   | 0     | 2.03   |
| Mushkin   | MKNSSDTR1TB-3DL    | 1 TB   | 8E6DF242A9AE | 734   | 0     | 2.01   |
| Samsung   | SSD 750 EVO        | 250 GB | 72BC89D9FD2F | 734   | 0     | 2.01   |
| Samsung   | SSD 840 EVO        | 1 TB   | D7E529879A58 | 727   | 0     | 1.99   |
| Samsung   | MZMPC032HBCD-00000 | 32 GB  | 220E1467B379 | 721   | 0     | 1.98   |
| Kingston  | SV300S37A120G      | 120 GB | 7E41E37286E7 | 709   | 0     | 1.94   |
| SanDisk   | SSD U110           | 16 GB  | 1F52FCED86B7 | 705   | 0     | 1.93   |
| Samsung   | SSD 850 EVO        | 250 GB | 5B7443313255 | 702   | 0     | 1.92   |
| Samsung   | SSD 850 EVO M.2    | 500 GB | 3C3646DE714D | 697   | 0     | 1.91   |
| Samsung   | SSD 850 EVO        | 500 GB | 7C1E9FF30B4F | 693   | 0     | 1.90   |
| Micron    | 1100_MTFDDAK256TBN | 256 GB | DC991C0655B5 | 687   | 0     | 1.88   |
| Micron    | 1100_MTFDDAK256TBN | 256 GB | 609D16207464 | 687   | 0     | 1.88   |
| Samsung   | MZ7PC128HAFU-000L1 | 128 GB | 0F321C63715D | 683   | 0     | 1.87   |
| SanDisk   | Ultra II           | 480 GB | 6E56DA9E59CE | 676   | 0     | 1.85   |
| Samsung   | SSD 860 EVO        | 2 TB   | 9A755AFDAB81 | 674   | 0     | 1.85   |
| OWC       | Mercury Extreme... | 240 GB | A4F067AC577B | 657   | 0     | 1.80   |
| WDC       | WDS250G1B0A-00H9H0 | 250 GB | A46BACCC9021 | 651   | 0     | 1.78   |
| Kingston  | SMSM150S324G2      | 24 GB  | F66155B9B455 | 650   | 0     | 1.78   |
| Crucial   | CT120M500SSD1      | 120 GB | 32E32E9A3AE9 | 644   | 0     | 1.77   |
| Samsung   | SSD 850 EVO        | 1 TB   | D90BEB820BB4 | 637   | 0     | 1.75   |
| Samsung   | SSD 860 EVO        | 1 TB   | 966C7B19A935 | 636   | 0     | 1.74   |
| Phison    | SATA SSD           | 16 GB  | 96E485339028 | 622   | 0     | 1.71   |
| Kingston  | SA400S37120G       | 120 GB | F7BF4980A89C | 617   | 0     | 1.69   |
| SPCC      | SSD                | 1 TB   | 12B685A62CF8 | 617   | 0     | 1.69   |
| SanDisk   | SDSSDHII120G       | 120 GB | A59BE28A870D | 613   | 0     | 1.68   |
| Samsung   | SSD 860 EVO        | 250 GB | 6C43091A2FAE | 611   | 0     | 1.68   |
| Intel     | SSDSA2CW160G3      | 160 GB | 3F9276F79CE0 | 610   | 0     | 1.67   |
| SanDisk   | SDSSDXP120G        | 120 GB | A0730F78B354 | 608   | 0     | 1.67   |
| Samsung   | SSD 860 EVO M.2    | 2 TB   | B93CF78FD433 | 607   | 0     | 1.66   |
| Kingston  | SA400S37240G       | 240 GB | 18E2BB646222 | 603   | 0     | 1.65   |
| Kingston  | SA400S37240G       | 240 GB | 2B1BCFA26548 | 603   | 0     | 1.65   |
| Intel     | SSDSC2BF180A4L     | 180 GB | D325BDBB71AF | 598   | 0     | 1.64   |
| Samsung   | MZNLN256HCHP-000L7 | 256 GB | 7CE95FC8A056 | 595   | 0     | 1.63   |
| Crucial   | M4-CT064M4SSD2     | 64 GB  | AF1883973174 | 589   | 0     | 1.61   |
| Hoodisk   | SSD                | 32 GB  | EE65DE21DD90 | 587   | 0     | 1.61   |
| Phison    | SATA SSD           | 16 GB  | 4053A8FB084B | 586   | 0     | 1.61   |
| Kingston  | SUV500MS240G       | 240 GB | 3D414073FCD9 | 580   | 0     | 1.59   |
| Samsung   | SSD 840 EVO        | 250 GB | D1206ACBB221 | 570   | 0     | 1.56   |
| SanDisk   | Ultra II           | 480 GB | 0DFE6961E0D7 | 567   | 0     | 1.56   |
| Samsung   | MZ7TE256HMHP-000H1 | 256 GB | EAA19A4D16F8 | 544   | 0     | 1.49   |
| Samsung   | SSD 850 EVO        | 500 GB | 2599AC16B848 | 540   | 0     | 1.48   |
| Samsung   | SSD 850 EVO        | 1 TB   | BEB64C2912ED | 2157  | 3     | 1.48   |
| Kingston  | SUV500MS120G       | 120 GB | D4B1F5E7CCD0 | 533   | 0     | 1.46   |
| Kingston  | SUV500MS120G       | 120 GB | 2A70CE3BE58B | 527   | 0     | 1.45   |
| Samsung   | SSD 850 EVO        | 500 GB | D8B7C9E330D7 | 525   | 0     | 1.44   |
| Kingston  | SUV500MS240G       | 240 GB | 8138DED579CB | 517   | 0     | 1.42   |
| Samsung   | SSD 850 EVO mSATA  | 250 GB | 92EC8E6BBD4B | 516   | 0     | 1.42   |
| Samsung   | MZ7TY256HDHP-000L7 | 256 GB | AE95838BEEBD | 516   | 0     | 1.41   |
| Samsung   | SSD 860 EVO        | 1 TB   | 3805D45E8E42 | 515   | 0     | 1.41   |
| Samsung   | SSD 850 PRO        | 256 GB | AA6A09CEE6FA | 509   | 0     | 1.40   |
| Samsung   | SSD 850 EVO        | 500 GB | 977CC4C4922C | 504   | 0     | 1.38   |
| Kingston  | SV300S37A480G      | 480 GB | 9D43C106D51A | 501   | 0     | 1.37   |
| Samsung   | SSD 860 EVO        | 1 TB   | 48F257F953F5 | 500   | 0     | 1.37   |
| Kingston  | SA400S37240G       | 240 GB | 09EE000D96B9 | 493   | 0     | 1.35   |
| Plextor   | PX-128M7VC         | 128 GB | 38EE83F81282 | 483   | 0     | 1.32   |
| Samsung   | MZ7LN512HMJP-000L7 | 512 GB | F794C0122AE8 | 483   | 0     | 1.32   |
| SPCC      | SSD                | 64 GB  | BE2BE3E86A9B | 482   | 0     | 1.32   |
| Patriot   | Burst              | 240 GB | 419360EC0C4A | 467   | 0     | 1.28   |
| Samsung   | SSD 850 PRO        | 256 GB | 6DDA7383BEAE | 467   | 0     | 1.28   |
| Samsung   | SSD 860 EVO mSATA  | 500 GB | 6A3DCD20B851 | 453   | 0     | 1.24   |
| Kingston  | SA400S37240G       | 240 GB | 00AF5AFB3B90 | 447   | 0     | 1.23   |
| Kingston  | SA400S37240G       | 240 GB | 8C50BDB364A7 | 447   | 0     | 1.23   |
| Kingston  | SA400S37240G       | 240 GB | 94EB6942C702 | 447   | 0     | 1.23   |
| Kingston  | SA400S37240G       | 240 GB | C44917288A1F | 447   | 0     | 1.23   |
| Samsung   | SSD 850 EVO        | 250 GB | 85F887DE5917 | 445   | 0     | 1.22   |
| AEGO      | SSD                | 120 GB | DD401F7D44A3 | 445   | 0     | 1.22   |
| Samsung   | SSD PM830 FDE 2... | 256 GB | 41772AC62177 | 440   | 0     | 1.21   |
| Samsung   | SSD 850 EVO        | 250 GB | C65EBB8F793A | 439   | 0     | 1.21   |
| Kingston  | SA400S37120G       | 120 GB | B0F77FFC6BDB | 428   | 0     | 1.17   |
| Vaseky    | V800-60G           | 64 GB  | 9D38D82F0C6A | 426   | 0     | 1.17   |
| SanDisk   | SD8SN8U128G1002    | 128 GB | 027B5A09BF54 | 424   | 0     | 1.16   |
| Samsung   | SSD 850 EVO        | 250 GB | 0A8E4179EFD0 | 423   | 0     | 1.16   |
| Toshiba   | THNSNJ128GCST      | 128 GB | E362BCAB2D17 | 422   | 0     | 1.16   |
| Crucial   | CT525MX300SSD1     | 528 GB | 33662F8248F3 | 417   | 0     | 1.14   |
| SanDisk   | SDSSDH3512G        | 512 GB | A6E2CD818FBA | 415   | 0     | 1.14   |
| SanDisk   | SSD PLUS           | 120 GB | 24CC567EACA2 | 413   | 0     | 1.13   |
| SanDisk   | SSD PLUS           | 120 GB | 9304CC34F218 | 413   | 0     | 1.13   |
| Intel     | SSDSC2BB120G4      | 120 GB | E7DD27B01C36 | 413   | 0     | 1.13   |
| Transcend | TS512GSSD370       | 512 GB | AFAAE4670D15 | 412   | 0     | 1.13   |
| Samsung   | SSD 850 EVO        | 500 GB | 6460D1C29DE8 | 410   | 0     | 1.12   |
| Samsung   | SSD 840 EVO        | 250 GB | 568A4515E0F7 | 407   | 0     | 1.12   |
| Hoodisk   | SSD                | 32 GB  | B03D1061FAB9 | 404   | 0     | 1.11   |
| SanDisk   | SSD PLUS           | 120 GB | 15403C3CA6C4 | 401   | 0     | 1.10   |
| SanDisk   | SSD PLUS           | 120 GB | 7E9491C9E901 | 401   | 0     | 1.10   |
| Corsair   | Force LS SSD       | 64 GB  | F346B576AEC9 | 396   | 0     | 1.09   |
| SanDisk   | SD6SB1M064G1022I   | 64 GB  | 541010E65282 | 396   | 0     | 1.09   |
| SanDisk   | SSD PLUS           | 120 GB | 3FE2165E2418 | 393   | 0     | 1.08   |
| SanDisk   | SSD PLUS           | 120 GB | 44FB211EE9FC | 393   | 0     | 1.08   |
| Kingston  | SA400S37120G       | 120 GB | 193ADC9D213D | 389   | 0     | 1.07   |
| Crucial   | CT1050MX300SSD1    | 1 TB   | 90304623F990 | 388   | 0     | 1.06   |
| Kingston  | SMS200S3120G       | 120 GB | 6821986AE169 | 385   | 0     | 1.06   |
| Samsung   | SSD PM830 mSATA    | 128 GB | 7246C2813CF1 | 385   | 0     | 1.06   |
| Kingston  | SVP200S37A60G      | 64 GB  | 3077A09A1BD5 | 377   | 0     | 1.03   |
| SPCC      | SSD                | 64 GB  | CE6CB449DFD6 | 366   | 0     | 1.01   |
| Samsung   | SSD 850 EVO        | 1 TB   | 5277D70B7CAA | 362   | 0     | 0.99   |
| Phison    | SATA SSD           | 16 GB  | 21F4E5F525C7 | 358   | 0     | 0.98   |
| Hoodisk   | SSD                | 64 GB  | F5CCE3582FD2 | 358   | 0     | 0.98   |
| Samsung   | SSD 850 EVO        | 120 GB | EEF3B0DD1A68 | 358   | 0     | 0.98   |
| Toshiba   | THNSFJ256GCSU      | 256 GB | 29F33C1182BB | 355   | 0     | 0.97   |
| Kingston  | RBUSNS8180S3128GI  | 128 GB | C9EC8A2FC108 | 351   | 0     | 0.96   |
| Intel     | SSDSC2BB480G7      | 480 GB | CF4E675990BE | 703   | 1     | 0.96   |
| Crucial   | CT250MX500SSD1     | 250 GB | 0642B30AE5D3 | 348   | 0     | 0.95   |
| Samsung   | SSD 860 EVO mSATA  | 1 TB   | 83B18AA5D07A | 346   | 0     | 0.95   |
| Crucial   | CT500MX200SSD1     | 500 GB | 4769EA7F02F4 | 345   | 0     | 0.95   |
| Crucial   | CT250MX500SSD1     | 250 GB | 8CA143458074 | 339   | 0     | 0.93   |
| Intel     | SSDSA2M080G2GC     | 80 GB  | 27C6CB1E583A | 3057  | 8     | 0.93   |
| Kingston  | SHFS37A120G        | 120 GB | 683601F988A2 | 334   | 0     | 0.92   |
| Intel     | SSDSA2CW120G3      | 120 GB | 76CDFA067F50 | 326   | 0     | 0.89   |
| Phison    | SATA SSD           | 128 GB | F59C196879E7 | 316   | 0     | 0.87   |
| Crucial   | CT250MX500SSD1     | 250 GB | E9AF292EACEE | 316   | 0     | 0.87   |
| PNY       | CS900 120GB SSD    | 120 GB | C3B0D4C00723 | 315   | 0     | 0.86   |
| PNY       | CS900 120GB SSD    | 120 GB | 2A05F3C8B743 | 314   | 0     | 0.86   |
| Samsung   | SSD 850 PRO        | 512 GB | D1E0E8DBCF5A | 306   | 0     | 0.84   |
| KingDian  | S400               | 120 GB | F40247E735D4 | 303   | 0     | 0.83   |
| OCZ       | VERTEX4            | 128 GB | E31AB8CAE6AC | 1188  | 3     | 0.81   |
| Intel     | SSDSC2CT240A3      | 240 GB | 9296A24F7E2B | 2370  | 7     | 0.81   |
| WDC       | WDS240G2G0B-00EPW0 | 240 GB | 4A63FE07F552 | 293   | 0     | 0.80   |
| Kingston  | SA400S37120G       | 120 GB | DAB5F335E37A | 289   | 0     | 0.79   |
| Samsung   | SSD 860 PRO        | 256 GB | B9571B379337 | 279   | 0     | 0.76   |
| SanDisk   | SD9SN8W128G1002    | 128 GB | 54E3DAC9956A | 278   | 0     | 0.76   |
| Intel     | SSDSC2CW060A3      | 64 GB  | 300ED56583C9 | 274   | 0     | 0.75   |
| Kingston  | SMS200S330G        | 32 GB  | 885B653BEE71 | 1369  | 4     | 0.75   |
| Phison    | SATA SSD           | 120 GB | 83F78741AEFA | 273   | 0     | 0.75   |
| SanDisk   | SD6SB2M512G1022I   | 512 GB | 951ECDA9602F | 536   | 1     | 0.74   |
| SK hynix  | HFS128G32MND-2200A | 128 GB | D97E579A738F | 798   | 2     | 0.73   |
| Toshiba   | THNSFJ256GCSU      | 256 GB | 8C62C3BCB539 | 261   | 0     | 0.72   |
| Kingston  | SV300S37A120G      | 120 GB | C60D095CB753 | 250   | 0     | 0.69   |
| Samsung   | SSD 860 EVO        | 500 GB | DD1891C7B2F2 | 246   | 0     | 0.68   |
| SanDisk   | SD5SG2256G1052E    | 256 GB | CCB49E73BA1A | 246   | 0     | 0.68   |
| Samsung   | SSD 840 PRO Series | 256 GB | EC680A1F4032 | 243   | 0     | 0.67   |
| Samsung   | SSD 860 EVO M.2    | 2 TB   | 8A0229FE339C | 242   | 0     | 0.67   |
| Kingston  | SUV500240G         | 240 GB | B5E9033D95CE | 241   | 0     | 0.66   |
| Samsung   | SSD 860 EVO        | 250 GB | BB75FBD18E8A | 240   | 0     | 0.66   |
| Samsung   | SSD 850 EVO        | 500 GB | D3FFE757AFB6 | 238   | 0     | 0.65   |
| Intel     | SSDSA2M160G2GC     | 160 GB | 11FA7399103B | 472   | 1     | 0.65   |
| WDC       | WDS500G2B0A        | 500 GB | 282A1B488010 | 234   | 0     | 0.64   |
| SK hynix  | HFS128G39TND-N210A | 128 GB | 69D1570B5252 | 232   | 0     | 0.64   |
| Plextor   | PX-256M8VG         | 256 GB | 5FED27AFDF95 | 230   | 0     | 0.63   |
| Kingston  | SMS200S3120G       | 120 GB | 9A64D4DA27ED | 1145  | 4     | 0.63   |
| SPCC      | SSD                | 120 GB | F5A5BA4843D2 | 228   | 0     | 0.63   |
| Samsung   | SSD 840 Series     | 250 GB | 1EFAF30D92C0 | 227   | 0     | 0.62   |
| Crucial   | CT1000MX500SSD1    | 1 TB   | F00315980158 | 226   | 0     | 0.62   |
| WDC       | WDS500G2B0A-00SM50 | 500 GB | 1CC6CB6F8CD3 | 223   | 0     | 0.61   |
| Kingston  | SMS200S3120G       | 120 GB | 4B774EA8E43E | 1112  | 4     | 0.61   |
| Samsung   | MZHPV512HDGL-000L1 | 512 GB | CC7C184273CD | 222   | 0     | 0.61   |
| Kingston  | SA400S37480G       | 480 GB | D8DD5546501C | 217   | 0     | 0.59   |
| Crucial   | CT275MX300SSD4     | 275 GB | 10521A74063E | 213   | 0     | 0.59   |
| Kingston  | SH103S3120G        | 120 GB | F91A88D46918 | 210   | 0     | 0.58   |
| OCZ       | VERTEX4            | 64 GB  | F1FDDFE2CFC7 | 206   | 0     | 0.57   |
| Samsung   | MZ7LN256HCHP-000L7 | 256 GB | 24DBE21D69B6 | 204   | 0     | 0.56   |
| SanDisk   | X400 M.2 2280      | 512 GB | 58E7285C01FA | 204   | 0     | 0.56   |
| Kingston  | SUV400S37240G      | 240 GB | BAD907548417 | 203   | 0     | 0.56   |
| WDC       | WDS500G1B0A-00H9H0 | 500 GB | D48760C20919 | 196   | 0     | 0.54   |
| Crucial   | CT525MX300SSD1     | 528 GB | 2DCB44CC2F04 | 585   | 2     | 0.54   |
| Samsung   | SSD 860 EVO mSATA  | 500 GB | A5EBC25744B2 | 188   | 0     | 0.52   |
| Intel     | SSDSC2BW240H6      | 240 GB | BE5607EDB9FB | 188   | 0     | 0.52   |
| Apacer    | 128GB SATA Flas... | 128 GB | 2D757B42D17F | 187   | 0     | 0.51   |
| Toshiba   | TR200              | 240 GB | 23C5FBA0B931 | 185   | 0     | 0.51   |
| Samsung   | SSD 860 EVO        | 500 GB | 916C6BDA8A20 | 183   | 0     | 0.50   |
| Samsung   | SSD 860 EVO        | 1 TB   | B1B1CEF2626E | 181   | 0     | 0.50   |
| China     | SSD 128G           | 128 GB | 72A7DA338B7F | 181   | 0     | 0.50   |
| Kingston  | SM2280S3G2240G     | 240 GB | D698F2818BA5 | 181   | 0     | 0.50   |
| KingDian  | S280-240GB         | 240 GB | 8B08F9B7B7DC | 177   | 0     | 0.49   |
| Crucial   | CT250MX200SSD1     | 250 GB | 3154BEC2E922 | 519   | 2     | 0.47   |
| Goodram   | SSD                | 120 GB | 859EFFD99FC2 | 172   | 0     | 0.47   |
| Apple     | SSD SM0512G        | 500 GB | A25925A03639 | 170   | 0     | 0.47   |
| Crucial   | CT250MX500SSD1     | 250 GB | 4186417EF027 | 168   | 0     | 0.46   |
| Kingston  | SMS200S330G        | 32 GB  | 6382D7408EB6 | 167   | 0     | 0.46   |
| ZTC       | SM201-064G         | 64 GB  | FC57C5559A07 | 164   | 0     | 0.45   |
| Samsung   | SSD 850 EVO M.2    | 250 GB | 7AB384160713 | 162   | 0     | 0.44   |
| Vaseky    | 128GV800           | 128 GB | 477BF0DEE340 | 161   | 0     | 0.44   |
| Apacer    | 32GB SATA Flash... | 32 GB  | 5A5821D64FCA | 1769  | 10    | 0.44   |
| Samsung   | SSD 860 EVO mSATA  | 500 GB | C6E458C8625C | 158   | 0     | 0.44   |
| Samsung   | SSD 860 EVO mSATA  | 500 GB | C3B13AEA4B16 | 158   | 0     | 0.44   |
| Samsung   | SSD PM871b M.2 ... | 256 GB | 11F61578CED6 | 158   | 0     | 0.43   |
| Samsung   | SSD 860 EVO mSATA  | 500 GB | F2E93C9581CD | 158   | 0     | 0.43   |
| Samsung   | SSD 860 QVO        | 1 TB   | 6E443A961811 | 157   | 0     | 0.43   |
| Intel     | SSDSC2BF180A4L     | 180 GB | A70C1A0A6BB4 | 472   | 2     | 0.43   |
| China     | 40GB SATA Flash... | 40 GB  | 31D6CE2C015C | 156   | 0     | 0.43   |
| Kingston  | SUV500240G         | 240 GB | BFA04D58BE1F | 156   | 0     | 0.43   |
| Crucial   | CT250MX500SSD1     | 250 GB | B48BA31A1312 | 154   | 0     | 0.42   |
| SK hynix  | SC311 SATA         | 512 GB | 087D47734039 | 153   | 0     | 0.42   |
| Samsung   | SSD 850 EVO        | 120 GB | 747473535E95 | 153   | 0     | 0.42   |
| Intel     | SSDSCKGF180A4L     | 180 GB | 5163461B5BA4 | 152   | 0     | 0.42   |
| Samsung   | SSD 860 QVO        | 1 TB   | 1FD0BB72CE90 | 150   | 0     | 0.41   |
| Transcend | TS256GSSD230S      | 256 GB | 0B66BE1B7774 | 147   | 0     | 0.40   |
| Transcend | TS240GMTS420S      | 240 GB | 0600F99C7A91 | 145   | 0     | 0.40   |
| Phison    | SATA SSD           | 32 GB  | D2BA40A2CA7A | 145   | 0     | 0.40   |
| Samsung   | SSD 860 EVO M.2    | 500 GB | 90ABFCAB8A68 | 144   | 0     | 0.40   |
| Lite-On   | CV8-8E256-11 SATA  | 256 GB | F8114A66B65F | 143   | 0     | 0.39   |
| Samsung   | SSD 860 QVO        | 4 TB   | A7F9811844BB | 141   | 0     | 0.39   |
| KingSpec  | NT-128             | 128 GB | 62AC25420CF5 | 140   | 0     | 0.38   |
| SanDisk   | SD6SB1M128G1022I   | 128 GB | EAB027D59183 | 139   | 0     | 0.38   |
| Samsung   | SSD 850 EVO        | 500 GB | F14082D7E628 | 138   | 0     | 0.38   |
| Samsung   | SSD 850 PRO        | 256 GB | 8CAB679EA8F9 | 136   | 0     | 0.37   |
| Kingston  | SA400S37480G       | 480 GB | 63D28D46694E | 135   | 0     | 0.37   |
| Samsung   | MZHPV256HDGL-000L1 | 256 GB | BCC170651BDD | 131   | 0     | 0.36   |
| SanDisk   | SD7SN3Q128G1002    | 128 GB | E98B01339936 | 131   | 0     | 0.36   |
| Kingston  | SV300S37A120G      | 120 GB | DF7BA0A9A24B | 651   | 4     | 0.36   |
| Crucial   | CT250MX200SSD1     | 250 GB | 1DBA3C9B0868 | 130   | 0     | 0.36   |
| Samsung   | SSD 860 QVO        | 4 TB   | 2B596E2EFA69 | 129   | 0     | 0.35   |
| WDC       | WDS480G2G0A-00JH30 | 480 GB | 851ED0A48527 | 129   | 0     | 0.35   |
| Hoodisk   | SSD                | 32 GB  | AAD008A412EC | 124   | 0     | 0.34   |
| Kingston  | SUV500MS240G       | 240 GB | 5E26F3088F64 | 123   | 0     | 0.34   |
| Lite-On   | LCS-128M6S-HP      | 128 GB | 2D86F5EEDBE4 | 123   | 0     | 0.34   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 6634C44D2667 | 123   | 0     | 0.34   |
| Apple     | SSD SM0128G        | 121 GB | 2704EDB33DE3 | 122   | 0     | 0.34   |
| Samsung   | SSD 850 EVO        | 500 GB | B858CDA8B779 | 121   | 0     | 0.33   |
| Kingston  | SA400S37240G       | 240 GB | BF513F51CD47 | 121   | 0     | 0.33   |
| SanDisk   | SDSSDH3 512G       | 512 GB | 71CBD3CF818F | 116   | 0     | 0.32   |
| Micron    | 1100_MTFDDAK256TBN | 256 GB | 270BD7E81CFA | 115   | 0     | 0.32   |
| Verbatim  | Vi500 S3 480GB SSD | 480 GB | 630F2F08C53A | 115   | 0     | 0.32   |
| KingDian  | S200               | 64 GB  | 9A00DBA6DE6D | 113   | 0     | 0.31   |
| Kingston  | SA400S37120G       | 120 GB | 2EA86D821DC9 | 113   | 0     | 0.31   |
| Crucial   | CT120BX300SSD1     | 120 GB | EBE522823790 | 108   | 0     | 0.30   |
| Samsung   | SSD 850 EVO        | 250 GB | FACEAA9AA15A | 107   | 0     | 0.30   |
| China     | OSSD256GBTSS2      | 256 GB | 941CABDA32A4 | 107   | 0     | 0.30   |
| China     | BR                 | 64 GB  | BC1C2A92A4F0 | 106   | 0     | 0.29   |
| Intel     | SSDSC2KG480G8R     | 480 GB | 41A8533CE536 | 105   | 0     | 0.29   |
| Intel     | SSDSC2KG480G8R     | 480 GB | B3E13B764123 | 105   | 0     | 0.29   |
| Patriot   | Burst              | 120 GB | BDF5C1DF85A2 | 104   | 0     | 0.28   |
| Samsung   | SSD 850 EVO        | 250 GB | 2CB3B3F06CFF | 103   | 0     | 0.28   |
| Phison    | SATA SSD           | 16 GB  | FB1A96831166 | 101   | 0     | 0.28   |
| Crucial   | CT525MX300SSD1     | 528 GB | 15A17F9A0137 | 300   | 2     | 0.27   |
| Samsung   | SSD 860 EVO mSATA  | 500 GB | 2C54922C3A6C | 100   | 0     | 0.27   |
| SanDisk   | SD9SN8W128G1102    | 128 GB | 47CEFBA093C0 | 98    | 0     | 0.27   |
| Samsung   | SSD 850 EVO M.2    | 250 GB | 472089CE4DFE | 98    | 0     | 0.27   |
| Crucial   | CT120M500SSD1      | 120 GB | D4905ECE6AA0 | 94    | 0     | 0.26   |
| Intel     | SSDSC2KG480G8      | 480 GB | 0E9ED257DA1F | 91    | 0     | 0.25   |
| Apple     | SSD SD0128F        | 121 GB | D5AE5AFFFA59 | 91    | 0     | 0.25   |
| PNY       | CS900 120GB SSD    | 120 GB | 471DA99EF7E8 | 90    | 0     | 0.25   |
| Intel     | SSDSC2BW240A4      | 240 GB | 2E8ACB1DA6B9 | 89    | 0     | 0.24   |
| Samsung   | SSD 860 EVO        | 1 TB   | 7181F7BD09D9 | 88    | 0     | 0.24   |
| Samsung   | SSD 860 QVO        | 2 TB   | 0016CA43DC4F | 88    | 0     | 0.24   |
| OCZ       | AGILITY4           | 128 GB | 980B2080A099 | 176   | 1     | 0.24   |
| Kingston  | SUV500MS480G       | 480 GB | 91AF2AA641F2 | 86    | 0     | 0.24   |
| ADATA     | SU800              | 256 GB | 72A628ADEB20 | 85    | 0     | 0.24   |
| ADATA     | SP600              | 64 GB  | 1DF60D7BB059 | 84    | 0     | 0.23   |
| ADATA     | SU800              | 1 TB   | 046FB264893F | 83    | 0     | 0.23   |
| WDC       | WDS120G2G0A-00JH30 | 120 GB | A700D44C1D5D | 82    | 0     | 0.23   |
| Toshiba   | TR200              | 240 GB | B9DEFBBEAC7F | 77    | 0     | 0.21   |
| Samsung   | SSD 860 EVO        | 500 GB | 138CC007FB6F | 77    | 0     | 0.21   |
| Samsung   | SSD 860 EVO        | 250 GB | CB4B8BB46053 | 77    | 0     | 0.21   |
| Crucial   | CT500MX500SSD1     | 500 GB | 2BA2A8B5D5C7 | 77    | 0     | 0.21   |
| Plextor   | PX-256M5S          | 256 GB | 94A344E1746B | 76    | 0     | 0.21   |
| Transcend | TS64GMSA370        | 64 GB  | B9117C7BB6E8 | 75    | 0     | 0.21   |
| Kingston  | SMSM151S3128GD     | 128 GB | 381F9805791F | 598   | 7     | 0.21   |
| Crucial   | CT250MX500SSD1     | 250 GB | 3ADCE5DB44AA | 73    | 0     | 0.20   |
| SPCC      | SSD                | 256 GB | A783A66CB712 | 71    | 0     | 0.19   |
| Samsung   | SSD 860 EVO        | 4 TB   | E9F981F90C3C | 68    | 0     | 0.19   |
| Transcend | TS32GMSA370        | 32 GB  | 3DE8C94852F1 | 65    | 0     | 0.18   |
| Crucial   | CT120BX500SSD1     | 120 GB | 458A2966BDCF | 65    | 0     | 0.18   |
| Intel     | SSDSC2KG480G8      | 480 GB | 1095FB4D46A1 | 62    | 0     | 0.17   |
| OCZ       | VERTEX-TURBO       | 32 GB  | DE3DECA80714 | 246   | 3     | 0.17   |
| Samsung   | MZMTE1T0HMJH-00000 | 1 TB   | 5CDE597FB3B5 | 59    | 0     | 0.16   |
| Apple     | SSD SM0128G        | 121 GB | 43F3B60D0F07 | 59    | 0     | 0.16   |
| Intel     | SSDSC2KG480G8      | 480 GB | 2EE49B252EBE | 58    | 0     | 0.16   |
| SanDisk   | Ultra II           | 240 GB | AE68FBF25DBE | 58    | 0     | 0.16   |
| Phison    | SATA SSD           | 16 GB  | 393980199625 | 58    | 0     | 0.16   |
| Phison    | SATA SSD           | 16 GB  | 11EB82F90D47 | 58    | 0     | 0.16   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 1BE00C2482B7 | 56    | 0     | 0.15   |
| Crucial   | CT500MX200SSD1     | 500 GB | 85009BF49FBA | 54    | 0     | 0.15   |
| KingFast  | SSD                | 120 GB | F33194902F34 | 53    | 0     | 0.15   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 16AFA3811E7F | 52    | 0     | 0.14   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 300FCCA363E1 | 52    | 0     | 0.14   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 5820F94C4D11 | 52    | 0     | 0.14   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | EE40B61EE899 | 52    | 0     | 0.14   |
| Plextor   | PX-256M5S          | 256 GB | AE096C1A946D | 51    | 0     | 0.14   |
| KingDian  | S280               | 120 GB | 31E97C3A0BEF | 49    | 0     | 0.14   |
| ADATA     | SU630              | 480 GB | E1D62AE4C30E | 48    | 0     | 0.13   |
| Kingston  | SA400S37240G       | 240 GB | DF7AF439B527 | 48    | 0     | 0.13   |
| KingDian  | S200               | 64 GB  | 58F722E8E16A | 47    | 0     | 0.13   |
| Samsung   | SSD 860 EVO        | 500 GB | 061F4A4223CD | 47    | 0     | 0.13   |
| Crucial   | CT250MX500SSD4     | 250 GB | 23EC30ADBD8C | 47    | 0     | 0.13   |
| Samsung   | SSD 860 EVO        | 500 GB | 4F929D79666B | 46    | 0     | 0.13   |
| SanDisk   | SSD U110           | 16 GB  | 0A59E9B7CCCD | 46    | 0     | 0.13   |
| WDC       | WDS240G2G0B-00EPW0 | 240 GB | BF2E013147A1 | 44    | 0     | 0.12   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | F2688EE3689A | 87    | 1     | 0.12   |
| SanDisk   | SD6SB1M256G1022I   | 256 GB | EA2E00FA2DDB | 1754  | 39    | 0.12   |
| SanDisk   | SSD PLUS           | 240 GB | B7BE375DA826 | 85    | 1     | 0.12   |
| Samsung   | SSD 860 EVO        | 250 GB | 7D88E8129431 | 42    | 0     | 0.12   |
| SanDisk   | SSD PLUS           | 120 GB | 9C29907A7517 | 40    | 0     | 0.11   |
| OCZ       | TRION150           | 240 GB | B6C69DD5A8A2 | 40    | 0     | 0.11   |
| SanDisk   | SSD PLUS           | 120 GB | 179190B553A6 | 40    | 0     | 0.11   |
| PNY       | CS900 120GB SSD    | 120 GB | 08F51548E874 | 39    | 0     | 0.11   |
| Corsair   | Force LS SSD       | 64 GB  | 8B5DABA92083 | 39    | 0     | 0.11   |
| WDC       | WDS120G1G0B-00RC30 | 120 GB | 5FF93853A772 | 37    | 0     | 0.10   |
| SanDisk   | SSD PLUS           | 1 TB   | 0450D22DBB57 | 37    | 0     | 0.10   |
| Kingston  | SUV400S37 120G     | 120 GB | CC794A4A55F6 | 37    | 0     | 0.10   |
| Kingston  | SA400S37120G       | 120 GB | 786ACE0B1778 | 36    | 0     | 0.10   |
| Micron    | 1100 SATA          | 512 GB | 6467DB653292 | 36    | 0     | 0.10   |
| ADATA     | SU750              | 256 GB | 1B0890D14696 | 36    | 0     | 0.10   |
| Intel     | SSDSC2BF180A4L     | 180 GB | 0BA41799BE7E | 107   | 2     | 0.10   |
| SanDisk   | SD6SB1M256G1022I   | 256 GB | 0120C58507D9 | 1754  | 48    | 0.10   |
| ADATA     | SU650              | 120 GB | B0B54A408147 | 34    | 0     | 0.09   |
| Phison    | SATA SSD           | 64 GB  | C745EBB43FA9 | 33    | 0     | 0.09   |
| Samsung   | SSD 860 QVO        | 1 TB   | C31C0D0C41FB | 31    | 0     | 0.09   |
| Crucial   | CT525MX300SSD1     | 528 GB | 2D73EAF826E4 | 30    | 0     | 0.08   |
| Apacer    | AS350              | 256 GB | 0D4936C2F398 | 30    | 0     | 0.08   |
| Kingston  | SV300S37A120G      | 120 GB | 22A2648650A8 | 30    | 0     | 0.08   |
| Crucial   | CT240BX500SSD1     | 240 GB | 7A792A48EE6D | 30    | 0     | 0.08   |
| Crucial   | CT1000MX500SSD1    | 1 TB   | B6A01F602D5D | 28    | 0     | 0.08   |
| Crucial   | CT480M500SSD1      | 480 GB | 5A8BBB232F57 | 580   | 20    | 0.08   |
| WDC       | WDS480G2G0A-00JH30 | 480 GB | 6E4098FF7BB8 | 26    | 0     | 0.07   |
| WDC       | WDS120G2G0B-00EPW0 | 120 GB | F50CC136B652 | 26    | 0     | 0.07   |
| WDC       | WDS480G2G0A-00JH30 | 480 GB | 66C774DFDC19 | 25    | 0     | 0.07   |
| Kingston  | SHSS37A240G        | 240 GB | 0AEA6DBC70FA | 25    | 0     | 0.07   |
| Samsung   | SSD 860 EVO        | 250 GB | BAC2E2AE64CB | 24    | 0     | 0.07   |
| Kingston  | SA400S37240G       | 240 GB | 3B25BE3816E9 | 24    | 0     | 0.07   |
| KingFast  | SSD                | 512 GB | 34D77DD899FA | 23    | 0     | 0.06   |
| Kingston  | SA400S37120G       | 120 GB | E38BE391FC77 | 22    | 0     | 0.06   |
| China     | CS2246-M512        | 506 GB | EE92BB35CC8F | 22    | 0     | 0.06   |
| Transcend | TS512GMSA370       | 512 GB | DD17C1EF90DA | 21    | 0     | 0.06   |
| Crucial   | CT250MX500SSD1     | 250 GB | 5FBFEFF63244 | 21    | 0     | 0.06   |
| Micron    | MTFDDAV256MBF-1... | 256 GB | 7374477AE7D4 | 21    | 0     | 0.06   |
| KingFast  | SSD                | 120 GB | F4532353E7AC | 21    | 0     | 0.06   |
| SanDisk   | SSD i100           | 64 GB  | 405C1B9F0413 | 21    | 0     | 0.06   |
| China     | SATA SSD           | 256 GB | 99A5EC4BD523 | 21    | 0     | 0.06   |
| Transcend | TS32GMSA370        | 32 GB  | 112FE204AE81 | 20    | 0     | 0.06   |
| Samsung   | SSD 850 EVO        | 250 GB | F551B82EE8CE | 20    | 0     | 0.06   |
| Transcend | TS32GMSA370        | 32 GB  | A663634DCBE2 | 20    | 0     | 0.06   |
| ORICO     | PH100-128G         | 128 GB | 9E8761F07F32 | 19    | 0     | 0.05   |
| Transcend | TS64GMSA370        | 64 GB  | 596242075497 | 19    | 0     | 0.05   |
| Samsung   | SSD 850 EVO        | 250 GB | 9489C78A2D56 | 17    | 0     | 0.05   |
| Kingston  | SA400S37240G       | 240 GB | 356E2880F7FA | 17    | 0     | 0.05   |
| Crucial   | CT240BX500SSD1     | 240 GB | 15C9EE9AC796 | 16    | 0     | 0.05   |
| WDC       | WDS500G2B0A        | 500 GB | EF5676FEAA52 | 15    | 0     | 0.04   |
| Intenso   | SSD Sata III       | 128 GB | C4BFD7CBB0A7 | 15    | 0     | 0.04   |
| Samsung   | MZNLN256HAJQ-000L7 | 256 GB | B8BC21B23C75 | 14    | 0     | 0.04   |
| Transcend | TS256GMTS400       | 256 GB | 93888CF64EF2 | 14    | 0     | 0.04   |
| Crucial   | CT120BX500SSD1     | 120 GB | 4E80C9ADAF10 | 14    | 0     | 0.04   |
| Intel     | SSDSC2BF180A5H REF | 180 GB | BBECD3464296 | 13    | 0     | 0.04   |
| Samsung   | SSD 860 EVO M.2    | 1 TB   | 2FDE46282F3B | 13    | 0     | 0.04   |
| PNY       | SSD2SC240G5LC70... | 240 GB | A3C586107375 | 13    | 0     | 0.04   |
| KingSpec  | NT-512             | 512 GB | 37B3F0BCAC4F | 12    | 0     | 0.04   |
| Crucial   | CT1000MX500SSD1    | 1 TB   | A06E3A95FED3 | 12    | 0     | 0.03   |
| Kingston  | SUV500MS480G       | 480 GB | DB1644A0BF24 | 12    | 0     | 0.03   |
| Lite-On   | LCH-256V2S         | 256 GB | 1AE30F48EB6D | 12    | 0     | 0.03   |
| Transcend | TS128GMSA370       | 128 GB | 0F2AD8FC60DF | 11    | 0     | 0.03   |
| Intenso   | SSD Sata III       | 240 GB | 7818E037BDB2 | 10    | 0     | 0.03   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 05DF5EC96C08 | 10    | 0     | 0.03   |
| Dogfish   | SSD                | 64 GB  | A6C7EE1F6C28 | 10    | 0     | 0.03   |
| Crucial   | CT2000MX500SSD1    | 2 TB   | 551DE889495C | 10    | 0     | 0.03   |
| SanDisk   | SDSSDA120G         | 120 GB | CAD3BDB33221 | 10    | 0     | 0.03   |
| Leven     | JAJS600M512C       | 512 GB | F0E981EFBCAB | 9     | 0     | 0.03   |
| Kingston  | SUV500MS240G       | 240 GB | DECA51F7FB2E | 8     | 0     | 0.02   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 3C0A22C1C5FC | 8     | 0     | 0.02   |
| Transcend | TS128GMTS430S      | 128 GB | EFE5BC0610B6 | 8     | 0     | 0.02   |
| Transcend | TS240GMTS420S      | 240 GB | 65FA0BD8388E | 8     | 0     | 0.02   |
| Micron    | 1100 SATA          | 256 GB | D6FA6B783462 | 22    | 2     | 0.02   |
| Apple     | SSD SM256E         | 256 GB | 364C0A87EE92 | 809   | 122   | 0.02   |
| Lexar     | 256GB SSD          | 256 GB | 5B897C49F5CB | 6     | 0     | 0.02   |
| Samsung   | SSD 860 EVO        | 250 GB | C1D3EE61BD1F | 5     | 0     | 0.02   |
| Kingston  | SA400S37480G       | 480 GB | BC4CBE17D909 | 5     | 0     | 0.01   |
| Crucial   | M4-CT128M4SSD3     | 128 GB | 0658ECB38AA6 | 4     | 0     | 0.01   |
| Apacer    | AS350              | 128 GB | 11ED8ECC0E48 | 4     | 0     | 0.01   |
| Kingston  | SA400S37240G       | 240 GB | EA92C8280ECB | 4     | 0     | 0.01   |
| Team      | T253X1240G         | 240 GB | C9987957EDA1 | 4     | 0     | 0.01   |
| Kingston  | SHFS37A240G        | 240 GB | B85061AA5404 | 4     | 0     | 0.01   |
| Mushkin   | MKNSSDSR120GB      | 120 GB | 2C1AF67A0ACC | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 1984E43E44F1 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 19EFAD8CFF01 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 3532BB96FC46 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 3A0FCAE4488A | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 3DBDD94A7746 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 4298DF2EBA82 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 449AA7F96715 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 4FB843407880 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 612F294C7F78 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 696DC019A710 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 7E433B4CF0FC | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 8472603979D0 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 9F948F3B7FDA | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | A04EC822CE25 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | A2FF35985D3C | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | A4909B503BFB | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | A4CDD04360F0 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | BD6C2D32DC02 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | C01B0EBE4243 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | C928CC0370B4 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | D1A1C6C0D3C9 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | E8298EA3E785 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | F1154F5B0B16 | 3     | 0     | 0.01   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | F69DD43A07E3 | 3     | 0     | 0.01   |
| Innodisk  | 2.5" SATA SSD 3... | 128 GB | 5CD5F9D67771 | 3     | 0     | 0.01   |
| ADATA     | SU650              | 120 GB | 8D33D68B4B5F | 779   | 212   | 0.01   |
| Samsung   | SSD 860 EVO        | 500 GB | CEDE12893682 | 3     | 0     | 0.01   |
| OWC       | Aura               | 960 GB | 77E8894FE42F | 3     | 0     | 0.01   |
| OCZ       | VERTEX-TURBO       | 32 GB  | 2E4FB510C7A9 | 350   | 108   | 0.01   |
| Samsung   | SSD 860 EVO        | 500 GB | C575EF977144 | 3     | 0     | 0.01   |
| SK hynix  | SC210 mSATA        | 256 GB | EB20FA9237FC | 391   | 139   | 0.01   |
| Hoodisk   | SSD                | 64 GB  | 805B6F0810F7 | 2     | 0     | 0.01   |
| Intel     | SSDSC2CW060A3      | 64 GB  | C8C7E259545D | 2648  | 1018  | 0.01   |
| Transcend | TS120GMTS420S      | 120 GB | C0F25474C43E | 2     | 0     | 0.01   |
| Transcend | TS120GMTS420S      | 120 GB | B2D9FD0503D5 | 2     | 0     | 0.01   |
| Samsung   | SSD 860 EVO        | 250 GB | C019EF95EB5F | 2     | 0     | 0.01   |
| Kingston  | SV300S37A60G       | 64 GB  | 73CA1B556CBA | 1586  | 804   | 0.01   |
| SanDisk   | SSD PLUS           | 240 GB | 7AEB5B82A569 | 1     | 0     | 0.01   |
| Samsung   | SSD 860 EVO        | 1 TB   | 907BCFB41309 | 1     | 0     | 0.01   |
| Intel     | SSDSC2BA200G3T     | 200 GB | FDEFE99A1F99 | 1943  | 1028  | 0.01   |
| Samsung   | MZNLH512HALU-00000 | 512 GB | D692FC1C54FF | 1     | 0     | 0.01   |
| Intel     | SSDSC2BA200G3T     | 200 GB | 4B2E23F452AA | 1803  | 1028  | 0.00   |
| Intel     | SSDSCKKF256G8H     | 256 GB | 433753F7B246 | 144   | 89    | 0.00   |
| Samsung   | MZHPV512HDGL-00000 | 512 GB | 803BF178BE13 | 1     | 0     | 0.00   |
| Samsung   | SSD UM410 Serie... | 16 GB  | 9F6EB1E9B947 | 38    | 24    | 0.00   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 1662CD83859A | 1     | 0     | 0.00   |
| China     | MSATA 64GB SSD     | 64 GB  | 72B830673528 | 1     | 0     | 0.00   |
| Kingston  | SNS4151S316G       | 16 GB  | 07FB035F48EF | 1252  | 1022  | 0.00   |
| Intel     | SSDSC2CW120A3      | 120 GB | 9DB303E94AFE | 1235  | 1019  | 0.00   |
| Lite-On   | LCH-128V2S         | 128 GB | 730E94A464CF | 1     | 0     | 0.00   |
| SanDisk   | SDSSDH3 250G       | 250 GB | 4B895CB8825C | 0     | 0     | 0.00   |
| SanDisk   | SD7UB3Q256G1001    | 256 GB | DDD3C1DE40D2 | 84    | 100   | 0.00   |
| Kingston  | RBU-SNS4151S316GG2 | 16 GB  | DE1A5FFE2BCE | 0     | 0     | 0.00   |
| FORESEE   | 64GB SSD           | 64 GB  | 4178AED9DC98 | 0     | 0     | 0.00   |
| QUMO      | SSD                | 120 GB | 5D9955D3E147 | 0     | 0     | 0.00   |
| SanDisk   | SSD G5 BICS4       | 500 GB | 6E052E2FB189 | 0     | 0     | 0.00   |
| Zheino    | CHN-mSATAQ3-120    | 120 GB | 35C29408CEBF | 0     | 0     | 0.00   |
| Intel     | SSDSC2KW120H6      | 120 GB | F1E069764BE1 | 2     | 29    | 0.00   |
| Kingston  | SNS4151S332G       | 32 GB  | 69FE3C89C36E | 79    | 1022  | 0.00   |
| Micron    | MTFDDAK256MAM-1K12 | 256 GB | 33DAAD77F144 | 52    | 1010  | 0.00   |
| Intel     | SSDSC2KW480H6      | 480 GB | 6832CA9672D0 | 39    | 1010  | 0.00   |
| Micron    | MTFDDAT128MAM-1J2  | 128 GB | B68AB8915542 | 28    | 1587  | 0.00   |
| Intenso   | SSD Sata III       | 128 GB | 90B4CF80C489 | 0     | 51    | 0.00   |
