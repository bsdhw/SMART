Samsung Hard Drives
===================

This is a list of all tested Samsung hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Samsung   | HD753LJ            | 752 GB | 1       | 3468  | 0     | 9.50   |
| Samsung   | HD103SJ            | 1 TB   | 3       | 2850  | 1     | 6.29   |
| Samsung   | HD204UI            | 2 TB   | 3       | 1849  | 0     | 5.07   |
| Samsung   | HD501LJ            | 500 GB | 5       | 3372  | 2     | 4.87   |
| Samsung   | HD502HJ            | 500 GB | 1       | 1380  | 0     | 3.78   |
| Samsung   | SP2504C            | 250 GB | 1       | 1158  | 0     | 3.17   |
| Samsung   | HD103UJ            | 1 TB   | 5       | 1844  | 33    | 2.48   |
| Samsung   | HD154UI            | 1.5 TB | 2       | 2245  | 75    | 2.21   |
| Samsung   | HD160JJ-P          | 160 GB | 1       | 547   | 0     | 1.50   |
| Samsung   | HM160HI            | 160 GB | 1       | 989   | 2     | 0.90   |
| Samsung   | HD502HI            | 500 GB | 1       | 204   | 0     | 0.56   |
| Samsung   | HM320JI            | 320 GB | 1       | 258   | 9     | 0.07   |
| Samsung   | HM080HI            | 80 GB  | 1       | 627   | 56    | 0.03   |
| Samsung   | HM500JI            | 500 GB | 1       | 1050  | 210   | 0.01   |
| Samsung   | HM250JI            | 250 GB | 1       | 16    | 1010  | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Samsung   | SpinPoint F3           | 2      | 4       | 2482  | 1     | 5.67   |
| Samsung   | SpinPoint F4 EG (AF)   | 1      | 3       | 1849  | 0     | 5.07   |
| Samsung   | SpinPoint T166         | 1      | 5       | 3372  | 2     | 4.87   |
| Samsung   | SpinPoint F1 DT        | 2      | 6       | 2114  | 27    | 3.65   |
| Samsung   | SpinPoint P120         | 1      | 1       | 1158  | 0     | 3.17   |
| Samsung   | SpinPoint F2 EG        | 2      | 3       | 1565  | 50    | 1.66   |
| Samsung   | SpinPoint P80 SD       | 1      | 1       | 547   | 0     | 1.50   |
| Samsung   | SpinPoint M5           | 2      | 2       | 502   | 506   | 0.45   |
| Samsung   | SpinPoint M6           | 1      | 1       | 258   | 9     | 0.07   |
| Samsung   | SpinPoint M40/60/80    | 1      | 1       | 627   | 56    | 0.03   |
| Samsung   | SpinPoint M7           | 1      | 1       | 1050  | 210   | 0.01   |
