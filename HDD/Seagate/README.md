Seagate Hard Drives
===================

This is a list of all tested Seagate hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Seagate   | ST3120023A         | 120 GB | 1       | 5214  | 0     | 14.29  |
| Seagate   | ST3500630NS        | 500 GB | 3       | 2715  | 0     | 7.44   |
| Seagate   | ST32000645NS       | 2 TB   | 1       | 2713  | 0     | 7.43   |
| Seagate   | ST3160318AS        | 160 GB | 1       | 2290  | 0     | 6.27   |
| Seagate   | ST4000NM0033-9Z... | 4 TB   | 3       | 1988  | 0     | 5.45   |
| Seagate   | ST3320620A         | 320 GB | 1       | 1913  | 0     | 5.24   |
| Seagate   | ST1000NC001-1DY162 | 1 TB   | 1       | 1848  | 0     | 5.06   |
| Seagate   | ST2000DM001-1ER164 | 2 TB   | 1       | 1647  | 0     | 4.51   |
| Seagate   | ST2000NM0033-9Z... | 2 TB   | 2       | 1644  | 0     | 4.51   |
| Seagate   | ST3250318AS        | 250 GB | 1       | 1636  | 0     | 4.48   |
| Seagate   | ST91000640NS       | 1 TB   | 1       | 1630  | 0     | 4.47   |
| Seagate   | ST33000651AS       | 3 TB   | 1       | 1539  | 0     | 4.22   |
| Seagate   | ST31000524AS       | 1 TB   | 3       | 1501  | 0     | 4.11   |
| Seagate   | ST3500412AS        | 500 GB | 1       | 1499  | 0     | 4.11   |
| Seagate   | ST8000DM002-1YW112 | 8 TB   | 1       | 1475  | 0     | 4.04   |
| Seagate   | ST500DM002-1BD142  | 500 GB | 3       | 1450  | 0     | 3.97   |
| Seagate   | ST9250410AS        | 250 GB | 1       | 1400  | 0     | 3.84   |
| Seagate   | ST1000NM0055-1V... | 1 TB   | 1       | 1376  | 0     | 3.77   |
| Seagate   | ST2000NC001-1DY164 | 2 TB   | 4       | 1797  | 28    | 3.71   |
| Seagate   | ST8000AS0002-1N... | 8 TB   | 3       | 1348  | 0     | 3.69   |
| Seagate   | ST3500413AS        | 500 GB | 6       | 1721  | 2     | 3.53   |
| Seagate   | ST4000DM000-1F2168 | 4 TB   | 6       | 1267  | 0     | 3.47   |
| Seagate   | ST3000DM001-1ER166 | 3 TB   | 3       | 1220  | 0     | 3.34   |
| Seagate   | ST6000VN0041-2E... | 6 TB   | 1       | 1215  | 0     | 3.33   |
| Seagate   | ST320LM001 HN-M... | 320 GB | 2       | 1189  | 0     | 3.26   |
| Seagate   | ST4000LM016-1N2170 | 4 TB   | 1       | 1146  | 0     | 3.14   |
| Seagate   | ST250DM000-1BD141  | 250 GB | 1       | 1142  | 0     | 3.13   |
| Seagate   | ST3000DM001-1CH166 | 3 TB   | 4       | 1463  | 164   | 3.09   |
| Seagate   | ST500DM009-2DM14C  | 500 GB | 1       | 1122  | 0     | 3.08   |
| Seagate   | ST3000VX000-1ES166 | 3 TB   | 1       | 1107  | 0     | 3.03   |
| Seagate   | ST320VT000-1BS14C  | 320 GB | 1       | 1083  | 0     | 2.97   |
| Seagate   | ST3250312AS        | 250 GB | 2       | 1063  | 0     | 2.91   |
| Seagate   | ST3160023A         | 160 GB | 1       | 1056  | 0     | 2.89   |
| Seagate   | ST1000DM003-1CH162 | 1 TB   | 5       | 1032  | 0     | 2.83   |
| Seagate   | ST380817AS         | 80 GB  | 1       | 997   | 0     | 2.73   |
| Seagate   | ST380013AS         | 80 GB  | 1       | 2943  | 2     | 2.69   |
| Seagate   | ST4000NE0025-2E... | 4 TB   | 2       | 960   | 0     | 2.63   |
| Seagate   | ST2000DL003-9VT166 | 2 TB   | 7       | 1631  | 174   | 2.57   |
| Seagate   | ST500DM002-1ER14C  | 500 GB | 1       | 882   | 0     | 2.42   |
| Seagate   | ST3250310AS        | 250 GB | 1       | 881   | 0     | 2.42   |
| Seagate   | ST3320620AS        | 320 GB | 2       | 1023  | 86    | 2.38   |
| Seagate   | ST4000VX007-2DT166 | 4 TB   | 2       | 861   | 0     | 2.36   |
| Seagate   | ST320410A          | 20 GB  | 1       | 2579  | 2     | 2.36   |
| Seagate   | ST3500312CS        | 500 GB | 2       | 744   | 0     | 2.04   |
| Seagate   | ST5000DM000-1FK178 | 5 TB   | 2       | 731   | 0     | 2.01   |
| Seagate   | ST31000424CS       | 1 TB   | 1       | 721   | 0     | 1.98   |
| Seagate   | ST1000LM024 HN-... | 1 TB   | 6       | 829   | 3     | 1.95   |
| Seagate   | ST12000NM0007-2... | 12 TB  | 1       | 710   | 0     | 1.95   |
| Seagate   | ST3500418AS        | 500 GB | 4       | 784   | 1     | 1.92   |
| Seagate   | ST2000NM0011       | 2 TB   | 1       | 2080  | 2     | 1.90   |
| Seagate   | ST3320620NS        | 320 GB | 1       | 2076  | 2     | 1.90   |
| Seagate   | ST2000VN004-2E4164 | 2 TB   | 3       | 687   | 0     | 1.88   |
| Seagate   | ST8000VN0022-2E... | 8 TB   | 1       | 658   | 0     | 1.80   |
| Seagate   | ST2000DM001-1CH164 | 2 TB   | 7       | 975   | 166   | 1.78   |
| Seagate   | ST2000LM015-2E8174 | 2 TB   | 11      | 629   | 0     | 1.72   |
| Seagate   | ST8000VN0002-1Z... | 8 TB   | 1       | 625   | 0     | 1.71   |
| Seagate   | ST9250315AS        | 250 GB | 2       | 928   | 506   | 1.64   |
| Seagate   | ST1000VX000-1CU162 | 1 TB   | 1       | 596   | 0     | 1.63   |
| Seagate   | ST4000LM024-2AN17V | 4 TB   | 1       | 579   | 0     | 1.59   |
| Seagate   | ST6000DM003-2CY186 | 6 TB   | 5       | 574   | 0     | 1.57   |
| Seagate   | ST2000VN000-1HJ164 | 2 TB   | 3       | 551   | 0     | 1.51   |
| Seagate   | ST3000NM0033-9Z... | 3 TB   | 1       | 508   | 0     | 1.39   |
| Seagate   | ST1000LX015-1U7172 | 1 TB   | 1       | 467   | 0     | 1.28   |
| Seagate   | ST3250410AS        | 250 GB | 1       | 458   | 0     | 1.26   |
| Seagate   | ST1000DM003-1ER162 | 1 TB   | 5       | 537   | 7     | 1.16   |
| Seagate   | ST3000DM001-9YN166 | 3 TB   | 2       | 790   | 832   | 1.08   |
| Seagate   | ST8000DM004-2CX188 | 8 TB   | 4       | 393   | 0     | 1.08   |
| Seagate   | ST4000DM004-2CV104 | 4 TB   | 4       | 388   | 0     | 1.06   |
| Seagate   | ST380811AS         | 80 GB  | 1       | 383   | 0     | 1.05   |
| Seagate   | ST31500541AS       | 1.5 TB | 5       | 649   | 119   | 1.03   |
| Seagate   | ST1000DM010-2EP102 | 1 TB   | 8       | 465   | 15    | 0.98   |
| Seagate   | ST2000NE0025-2F... | 2 TB   | 2       | 338   | 0     | 0.93   |
| Seagate   | ST5000LM000-2AN170 | 5 TB   | 4       | 328   | 0     | 0.90   |
| Seagate   | ST3000DM008-2DM166 | 3 TB   | 3       | 678   | 24    | 0.88   |
| Seagate   | ST1000LM048-2E7172 | 1 TB   | 4       | 267   | 0     | 0.73   |
| Seagate   | ST3750640NS        | 752 GB | 2       | 2066  | 1043  | 0.71   |
| Seagate   | ST3120211AS        | 120 GB | 1       | 508   | 1     | 0.70   |
| Seagate   | ST380215A          | 80 GB  | 1       | 242   | 0     | 0.66   |
| Seagate   | ST500LM012 HN-M... | 500 GB | 2       | 193   | 0     | 0.53   |
| Seagate   | ST2000DL004 HD2... | 2 TB   | 1       | 180   | 0     | 0.49   |
| Seagate   | ST1000LM035-1RK172 | 1 TB   | 6       | 216   | 152   | 0.47   |
| Seagate   | ST3500320NS        | 500 GB | 1       | 1324  | 7     | 0.45   |
| Seagate   | ST2000DM006-2DM164 | 2 TB   | 1       | 164   | 0     | 0.45   |
| Seagate   | ST3120026A         | 120 GB | 1       | 1076  | 8     | 0.33   |
| Seagate   | ST4000VN008-2DR166 | 4 TB   | 4       | 115   | 0     | 0.32   |
| Seagate   | ST9120822AS        | 120 GB | 1       | 1028  | 8     | 0.31   |
| Seagate   | ST9500530NS 42D... | 500 GB | 2       | 221   | 48    | 0.31   |
| Seagate   | ST2000LM007-1R8174 | 2 TB   | 1       | 111   | 0     | 0.31   |
| Seagate   | ST3000VN007-2E4166 | 3 TB   | 2       | 101   | 0     | 0.28   |
| Seagate   | ST500LT012-1DG142  | 500 GB | 1       | 243   | 2     | 0.22   |
| Seagate   | ST2000DM008-2FR102 | 2 TB   | 3       | 118   | 48    | 0.18   |
| Seagate   | ST500LM030-2E717D  | 500 GB | 1       | 55    | 0     | 0.15   |
| Seagate   | ST3250820AS        | 250 GB | 1       | 392   | 7     | 0.13   |
| Seagate   | ST500LM021-1KJ152  | 500 GB | 3       | 342   | 710   | 0.09   |
| Seagate   | ST3000DM007-1WY10G | 3 TB   | 1       | 29    | 0     | 0.08   |
| Seagate   | ST95005620AS       | 500 GB | 1       | 360   | 19    | 0.05   |
| Seagate   | ST31000528AS       | 1 TB   | 1       | 17    | 0     | 0.05   |
| Seagate   | ST9250827AS        | 250 GB | 1       | 564   | 31    | 0.05   |
| Seagate   | ST9500325AS        | 500 GB | 1       | 2276  | 128   | 0.05   |
| Seagate   | ST8000VN004-2M2101 | 8 TB   | 6       | 12    | 0     | 0.03   |
| Seagate   | ST31500341AS       | 1.5 TB | 1       | 36    | 3     | 0.03   |
| Seagate   | ST500DM002-1BC142  | 500 GB | 1       | 450   | 78    | 0.02   |
| Seagate   | ST3160815A         | 160 GB | 1       | 4054  | 1048  | 0.01   |
| Seagate   | ST12000VN0008-2... | 12 TB  | 16      | 3     | 0     | 0.01   |
| Seagate   | ST3500514NS        | 500 GB | 1       | 205   | 103   | 0.01   |
| Seagate   | ST9500420AS        | 500 GB | 3       | 810   | 1095  | 0.00   |
| Seagate   | ST320LT007-9ZV142  | 320 GB | 2       | 555   | 1009  | 0.00   |
| Seagate   | ST500LT012-9WS142  | 500 GB | 3       | 485   | 1010  | 0.00   |
| Seagate   | ST2000DM001-9YN164 | 2 TB   | 1       | 234   | 760   | 0.00   |
| Seagate   | ST3320418AS        | 320 GB | 1       | 35    | 1086  | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Seagate   | Barracuda ATA V        | 1      | 1       | 5214  | 0     | 14.29  |
| Seagate   | Constellation ES.2 ... | 1      | 1       | 2713  | 0     | 7.43   |
| Seagate   | Constellation.2 (SATA) | 1      | 1       | 1630  | 0     | 4.47   |
| Seagate   | Constellation ES.3     | 3      | 6       | 1627  | 0     | 4.46   |
| Seagate   | Barracuda ES           | 3      | 6       | 2392  | 348   | 4.27   |
| Seagate   | Barracuda XT           | 1      | 1       | 1539  | 0     | 4.22   |
| Seagate   | Constellation CS       | 2      | 5       | 1808  | 23    | 3.98   |
| Seagate   | Enterprise Capacity... | 1      | 1       | 1376  | 0     | 3.77   |
| Seagate   | Archive HDD            | 1      | 3       | 1348  | 0     | 3.69   |
| Seagate   | Desktop HDD.15         | 3      | 9       | 1171  | 0     | 3.21   |
| Seagate   | Barracuda 7200.12      | 8      | 19      | 1267  | 58    | 3.04   |
| Seagate   | Video 2.5              | 1      | 1       | 1083  | 0     | 2.97   |
| Seagate   | Barracuda Green (AF)   | 1      | 7       | 1631  | 174   | 2.57   |
| Seagate   | Skyhawk                | 1      | 2       | 861   | 0     | 2.36   |
| Seagate   | U6                     | 1      | 1       | 2579  | 2     | 2.36   |
| Seagate   | Surveillance           | 2      | 2       | 851   | 0     | 2.33   |
| Seagate   | Barracuda 7200.14 (AF) | 12     | 34      | 1014  | 128   | 2.32   |
| Seagate   | Barracuda 7200.7 an... | 4      | 4       | 1518  | 3     | 2.16   |
| Seagate   | Pipeline HD 5900.2     | 2      | 3       | 736   | 0     | 2.02   |
| Seagate   | Exos X12               | 1      | 1       | 710   | 0     | 1.95   |
| Seagate   | SpinPoint M8 (AF)      | 3      | 10      | 774   | 2     | 1.93   |
| Seagate   | Constellation ES (S... | 1      | 1       | 2080  | 2     | 1.90   |
| Seagate   | Barracuda 7200.10      | 7      | 8       | 1248  | 154   | 1.81   |
| Seagate   | IronWolf Pro           | 2      | 4       | 649   | 0     | 1.78   |
| Seagate   | NAS HDD                | 2      | 4       | 569   | 0     | 1.56   |
| Seagate   | Barracuda LP           | 2      | 6       | 791   | 99    | 1.54   |
| Seagate   | Barracuda 2.5 5400     | 5      | 21      | 473   | 0     | 1.30   |
| Seagate   | FireCuda 2.5           | 1      | 1       | 467   | 0     | 1.28   |
| Seagate   | Momentus 5400.6        | 2      | 3       | 1377  | 380   | 1.11   |
| Seagate   | Barracuda 3.5          | 5      | 17      | 505   | 11    | 1.07   |
| Seagate   | BarraCuda 3.5          | 2      | 8       | 403   | 18    | 1.05   |
| Seagate   | Momentus 7200.4        | 2      | 4       | 958   | 821   | 0.96   |
| Seagate   | Barracuda Compute      | 2      | 5       | 320   | 0     | 0.88   |
| Seagate   | Barracuda 7200.9       | 2      | 2       | 446   | 1     | 0.87   |
| Seagate   | SpinPoint F4 EG (AF)   | 1      | 1       | 180   | 0     | 0.49   |
| Seagate   | Laptop HDD             | 4      | 8       | 484   | 645   | 0.46   |
| Seagate   | Barracuda ES.2         | 1      | 1       | 1324  | 7     | 0.45   |
| Seagate   | Mobile HDD             | 2      | 7       | 201   | 130   | 0.44   |
| Seagate   | IronWolf               | 7      | 33      | 143   | 0     | 0.39   |
| Seagate   | Momentus 5400.3        | 1      | 1       | 1028  | 8     | 0.31   |
| Seagate   | Constellation          | 1      | 2       | 221   | 48    | 0.31   |
| Seagate   | Momentus XT            | 1      | 1       | 360   | 19    | 0.05   |
| Seagate   | Momentus 5400.4        | 1      | 1       | 564   | 31    | 0.05   |
| Seagate   | Barracuda 7200.11      | 1      | 1       | 36    | 3     | 0.03   |
| Seagate   | Constellation ES (S... | 1      | 1       | 205   | 103   | 0.01   |
| Seagate   | Momentus Thin          | 1      | 2       | 555   | 1009  | 0.00   |
