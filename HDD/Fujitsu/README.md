Fujitsu Hard Drives
===================

This is a list of all tested Fujitsu hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Fujitsu   | MJA2160BH FFS G1   | 160 GB | 1       | 707   | 0     | 1.94   |
| Fujitsu   | MJA2160BH G2       | 160 GB | 1       | 633   | 0     | 1.74   |
| Fujitsu   | MHZ2320BH FFS G1   | 320 GB | 1       | 203   | 0     | 0.56   |
| Fujitsu   | MHS2040AT D        | 40 GB  | 1       | 1334  | 37    | 0.10   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Fujitsu   | MJA BH                 | 2      | 2       | 670   | 0     | 1.84   |
| Fujitsu   | MHZ BH                 | 1      | 1       | 203   | 0     | 0.56   |
| Fujitsu   | MHS AT                 | 1      | 1       | 1334  | 37    | 0.10   |
