Maxtor Hard Drives
==================

This is a list of all tested Maxtor hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Maxtor    | STM3250310AS       | 250 GB | 1       | 140   | 0     | 0.39   |
| Maxtor    | 6Y080P0            | 82 GB  | 2       | 9     | 7     | 0.01   |
| Maxtor    | 6E040L0            | 41 GB  | 1       | 20    | 1133  | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Maxtor    | DiamondMax 21          | 1      | 1       | 140   | 0     | 0.39   |
| Maxtor    | DiamondMax Plus 9      | 1      | 2       | 9     | 7     | 0.01   |
| Maxtor    | DiamondMax Plus 8      | 1      | 1       | 20    | 1133  | 0.00   |
