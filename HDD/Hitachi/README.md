Hitachi Hard Drives
===================

This is a list of all tested Hitachi hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Hitachi   | HDT721010SLA360    | 1 TB   | 1       | 3782  | 0     | 10.36  |
| Hitachi   | HDS725050KLA360    | 500 GB | 1       | 3763  | 0     | 10.31  |
| Hitachi   | HDT721032SLA360    | 320 GB | 1       | 3534  | 0     | 9.68   |
| Hitachi   | HUA7250SBSUN500G   | 500 GB | 2       | 2785  | 0     | 7.63   |
| Hitachi   | HUA722020ALA331    | 2 TB   | 2       | 2745  | 0     | 7.52   |
| Hitachi   | HDT725025VLA380    | 250 GB | 1       | 2680  | 0     | 7.34   |
| Hitachi   | HTS542525K9SA00    | 250 GB | 1       | 2643  | 0     | 7.24   |
| Hitachi   | HUA723020ALA641    | 2 TB   | 7       | 2259  | 0     | 6.19   |
| Hitachi   | HDS721616PLAT80    | 160 GB | 1       | 2187  | 0     | 5.99   |
| Hitachi   | HDS728080PLA380    | 82 GB  | 1       | 2086  | 0     | 5.72   |
| Hitachi   | HUA723020ALA640    | 2 TB   | 2       | 1944  | 0     | 5.33   |
| Hitachi   | HUA722020ALA330    | 2 TB   | 4       | 2204  | 91    | 4.84   |
| Hitachi   | HDS721032CLA362    | 320 GB | 1       | 1323  | 0     | 3.63   |
| Hitachi   | HDS721010KLA330    | 1 TB   | 1       | 3610  | 2     | 3.30   |
| Hitachi   | HTS545025B9SA02    | 250 GB | 1       | 1008  | 0     | 2.76   |
| Hitachi   | HDS723020BLA642    | 2 TB   | 1       | 1007  | 0     | 2.76   |
| Hitachi   | HDS723030ALA640    | 3 TB   | 1       | 828   | 0     | 2.27   |
| Hitachi   | HTS723225L9A360    | 250 GB | 1       | 644   | 0     | 1.77   |
| Hitachi   | HUA723030ALA640    | 3 TB   | 1       | 563   | 0     | 1.54   |
| Hitachi   | HTS725050A7E630    | 500 GB | 1       | 535   | 0     | 1.47   |
| Hitachi   | HTS541660J9SA00    | 64 GB  | 1       | 517   | 0     | 1.42   |
| Hitachi   | HTS547550A9E384    | 500 GB | 3       | 681   | 49    | 1.10   |
| Hitachi   | HTS723220L9A360    | 200 GB | 1       | 401   | 0     | 1.10   |
| Hitachi   | HDT725032VLA380    | 320 GB | 1       | 389   | 0     | 1.07   |
| Hitachi   | HTS721060G9SA00    | 64 GB  | 1       | 1545  | 3     | 1.06   |
| Hitachi   | HUS724030ALE641    | 3 TB   | 5       | 328   | 0     | 0.90   |
| Hitachi   | HTE723225A7A364    | 250 GB | 1       | 227   | 0     | 0.62   |
| Hitachi   | HDS721010CLA630    | 1 TB   | 1       | 678   | 2     | 0.62   |
| Hitachi   | HTS543232L9A300    | 320 GB | 1       | 223   | 0     | 0.61   |
| Hitachi   | HDS5C3030ALA630    | 3 TB   | 5       | 247   | 5     | 0.55   |
| Hitachi   | HTS543225L9A300    | 250 GB | 1       | 736   | 5     | 0.34   |
| Hitachi   | HUS724040ALE641    | 4 TB   | 8       | 134   | 1     | 0.33   |
| Hitachi   | HTS421260H9AT00    | 64 GB  | 1       | 1338  | 12    | 0.28   |
| Hitachi   | HUA721010KLA330    | 1 TB   | 1       | 52    | 0     | 0.14   |
| Hitachi   | HTS543232L9SA02    | 320 GB | 1       | 1954  | 37    | 0.14   |
| Hitachi   | HTS543232A7A384    | 320 GB | 2       | 198   | 512   | 0.14   |
| Hitachi   | HTS725025A9A364    | 250 GB | 1       | 548   | 11    | 0.13   |
| Hitachi   | HTS541060G9SA00    | 64 GB  | 1       | 255   | 6     | 0.10   |
| Hitachi   | HDP725050GLA360    | 500 GB | 1       | 1174  | 38    | 0.08   |
| Hitachi   | HTS541680J9SA00    | 80 GB  | 1       | 977   | 36    | 0.07   |
| Hitachi   | HTS543212L9A300    | 120 GB | 2       | 1802  | 550   | 0.02   |
| Hitachi   | HDP725016GLA380    | 160 GB | 1       | 1804  | 250   | 0.02   |
| Hitachi   | HDS721010CLA332    | 1 TB   | 1       | 1511  | 220   | 0.02   |
| Hitachi   | HTS725032A7E630    | 320 GB | 1       | 348   | 1025  | 0.00   |
| Hitachi   | HTS545050A7E380    | 500 GB | 1       | 93    | 345   | 0.00   |
| Hitachi   | DK23AA-12          | 12 GB  | 1       | 4     | 31    | 0.00   |
| Hitachi   | HTS545032B9A300    | 320 GB | 1       | 103   | 1024  | 0.00   |
| Hitachi   | HTS721060G9AT00    | 64 GB  | 1       | 0     | 37    | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Hitachi   | Deskstar 7K500         | 1      | 1       | 3763  | 0     | 10.31  |
| Hitachi   | Deskstar 7K1000.B      | 2      | 2       | 3658  | 0     | 10.02  |
| Hitachi   | Sun Internal           | 1      | 2       | 2785  | 0     | 7.63   |
| Hitachi   | Travelstar 5K250       | 1      | 1       | 2643  | 0     | 7.24   |
| Hitachi   | Deskstar 7K160         | 1      | 1       | 2187  | 0     | 5.99   |
| Hitachi   | Ultrastar A7K2000      | 2      | 6       | 2385  | 61    | 5.74   |
| Hitachi   | Deskstar 7K80          | 1      | 1       | 2086  | 0     | 5.72   |
| Hitachi   | Ultrastar 7K3000       | 3      | 10      | 2027  | 0     | 5.55   |
| Hitachi   | Deskstar T7K500        | 2      | 2       | 1535  | 0     | 4.21   |
| Hitachi   | Deskstar 7K1000        | 1      | 1       | 3610  | 2     | 3.30   |
| Hitachi   | Deskstar 7K3000        | 2      | 2       | 918   | 0     | 2.52   |
| Hitachi   | Travelstar 7K320       | 2      | 2       | 523   | 0     | 1.43   |
| Hitachi   | Deskstar 7K1000.C      | 3      | 3       | 1171  | 74    | 1.42   |
| Hitachi   | Travelstar 5K500.B     | 2      | 2       | 555   | 512   | 1.38   |
| Hitachi   | Travelstar 5K750       | 1      | 3       | 681   | 49    | 1.10   |
| Hitachi   | Travelstar 5K160       | 2      | 2       | 747   | 18    | 0.75   |
| Hitachi   | Travelstar Z7K500      | 2      | 2       | 442   | 513   | 0.73   |
| Hitachi   | Travelstar Z7K320      | 1      | 1       | 227   | 0     | 0.62   |
| Hitachi   | Ultrastar 7K4000       | 2      | 13      | 208   | 1     | 0.55   |
| Hitachi   | Deskstar 5K3000        | 1      | 5       | 247   | 5     | 0.55   |
| Hitachi   | Travelstar 7K100       | 2      | 2       | 772   | 20    | 0.53   |
| Hitachi   | Travelstar 4K120       | 1      | 1       | 1338  | 12    | 0.28   |
| Hitachi   | Travelstar 5K320       | 4      | 5       | 1304  | 229   | 0.23   |
| Hitachi   | Ultrastar A7K1000      | 1      | 1       | 52    | 0     | 0.14   |
| Hitachi   | Travelstar Z5K320      | 1      | 2       | 198   | 512   | 0.14   |
| Hitachi   | Travelstar 7K500       | 1      | 1       | 548   | 11    | 0.13   |
| Hitachi   | Travelstar 5K100       | 1      | 1       | 255   | 6     | 0.10   |
| Hitachi   | Deskstar P7K500        | 2      | 2       | 1489  | 144   | 0.05   |
| Hitachi   | Travelstar Z5K500      | 1      | 1       | 93    | 345   | 0.00   |
| Hitachi   | Travelstar DK23XX/D... | 1      | 1       | 4     | 31    | 0.00   |
