HGST Hard Drives
================

This is a list of all tested HGST hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| HGST      | HUS724020ALA640    | 2 TB   | 4       | 1983  | 0     | 5.43   |
| HGST      | HUH728060ALE600    | 6 TB   | 1       | 1820  | 0     | 4.99   |
| HGST      | HDN724030ALE640    | 3 TB   | 2       | 1543  | 0     | 4.23   |
| HGST      | HDN724040ALE640    | 4 TB   | 8       | 1683  | 101   | 3.15   |
| HGST      | HDN728080ALE604    | 8 TB   | 1       | 1005  | 0     | 2.75   |
| HGST      | HTS541010A9E680    | 1 TB   | 3       | 869   | 0     | 2.38   |
| HGST      | HTS721010A9E630    | 1 TB   | 3       | 499   | 0     | 1.37   |
| HGST      | HDN726040ALE614    | 4 TB   | 1       | 461   | 0     | 1.26   |
| HGST      | HUS722T2TALA604    | 2 TB   | 4       | 544   | 4     | 1.11   |
| HGST      | HUS726020ALA610    | 2 TB   | 1       | 381   | 0     | 1.04   |
| HGST      | HTS725050A7E630    | 500 GB | 4       | 659   | 256   | 0.72   |
| HGST      | HUS728T8TALE6L4    | 8 TB   | 2       | 233   | 0     | 0.64   |
| HGST      | HTS541075A7E630    | 752 GB | 2       | 321   | 507   | 0.62   |
| HGST      | HUH721010ALE604    | 10 TB  | 1       | 204   | 0     | 0.56   |
| HGST      | HTS545050A7E680    | 500 GB | 1       | 86    | 0     | 0.24   |
| HGST      | HUS724040ALA640    | 4 TB   | 2       | 85    | 0     | 0.23   |
| HGST      | HUS726040ALA610    | 4 TB   | 1       | 1058  | 78    | 0.04   |
| HGST      | HTS725025A7E630    | 250 GB | 1       | 43    | 20    | 0.01   |
| HGST      | HTS541010A7E630    | 1 TB   | 1       | 96    | 1023  | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| HGST      | Ultrastar He8          | 1      | 1       | 1820  | 0     | 4.99   |
| HGST      | Ultrastar 7K4000       | 2      | 6       | 1350  | 0     | 3.70   |
| HGST      | Deskstar NAS           | 4      | 12      | 1501  | 67    | 3.14   |
| HGST      | Travelstar 5K1000      | 1      | 3       | 869   | 0     | 2.38   |
| HGST      | Travelstar 7K1000      | 1      | 3       | 499   | 0     | 1.37   |
| HGST      | Ultrastar 7K2          | 1      | 4       | 544   | 4     | 1.11   |
| HGST      | Ultrastar DC HC320     | 1      | 2       | 233   | 0     | 0.64   |
| HGST      | Travelstar Z7K500      | 2      | 5       | 536   | 209   | 0.58   |
| HGST      | Ultrastar He10         | 1      | 1       | 204   | 0     | 0.56   |
| HGST      | Ultrastar 7K6000       | 2      | 2       | 720   | 39    | 0.54   |
| HGST      | Travelstar Z5K1000     | 2      | 3       | 246   | 679   | 0.42   |
| HGST      | Travelstar Z5K500      | 1      | 1       | 86    | 0     | 0.24   |
