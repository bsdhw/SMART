Toshiba Hard Drives
===================

This is a list of all tested Toshiba hard drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

Contents
--------

1. [ HDD by Model  ](#hdd-by-model)
2. [ HDD by Family ](#hdd-by-family)

HDD by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Toshiba   | DT01ACA200         | 2 TB   | 3       | 1992  | 0     | 5.46   |
| Toshiba   | MQ01ABD100H        | 1 TB   | 1       | 1970  | 0     | 5.40   |
| Toshiba   | MG03ACA200         | 2 TB   | 5       | 1631  | 0     | 4.47   |
| Toshiba   | MG04ACA400E        | 4 TB   | 2       | 1535  | 0     | 4.21   |
| Toshiba   | DT01ACA300         | 3 TB   | 8       | 1606  | 129   | 3.56   |
| Toshiba   | DT01ACA050         | 500 GB | 4       | 1146  | 1     | 2.96   |
| Toshiba   | MQ01UBB200         | 2 TB   | 1       | 1067  | 0     | 2.93   |
| Toshiba   | MC04ACA400E        | 4 TB   | 2       | 1023  | 0     | 2.80   |
| Toshiba   | HDWE150            | 5 TB   | 6       | 1186  | 5     | 2.73   |
| Toshiba   | MQ04UBD200         | 2 TB   | 1       | 906   | 0     | 2.48   |
| Toshiba   | DT01ACA100         | 1 TB   | 10      | 883   | 0     | 2.42   |
| Toshiba   | HDWD130            | 3 TB   | 4       | 656   | 0     | 1.80   |
| Toshiba   | MQ04UBF100         | 1 TB   | 1       | 592   | 0     | 1.62   |
| Toshiba   | HDWQ140            | 4 TB   | 4       | 410   | 0     | 1.12   |
| Toshiba   | HDWD120            | 2 TB   | 2       | 400   | 0     | 1.10   |
| Toshiba   | MK1517GAP          | 16 GB  | 1       | 380   | 0     | 1.04   |
| Toshiba   | MK3259GSXP         | 320 GB | 1       | 326   | 0     | 0.89   |
| Toshiba   | HDWD110            | 1 TB   | 8       | 202   | 0     | 0.55   |
| Toshiba   | MQ01ABD100         | 1 TB   | 5       | 286   | 143   | 0.52   |
| Toshiba   | MQ01ABF050         | 500 GB | 3       | 195   | 1     | 0.29   |
| Toshiba   | MK7575GSX          | 752 GB | 1       | 604   | 5     | 0.28   |
| Toshiba   | HDWN180            | 8 TB   | 3       | 87    | 0     | 0.24   |
| Toshiba   | MQ04ABF100         | 1 TB   | 6       | 85    | 1     | 0.18   |
| Toshiba   | MG04ACA600E        | 6 TB   | 1       | 28    | 0     | 0.08   |
| Toshiba   | DT01ABA300         | 3 TB   | 1       | 232   | 8     | 0.07   |
| Toshiba   | MK5061GSYN         | 500 GB | 1       | 15    | 0     | 0.04   |
| Toshiba   | MK3265GSXN         | 320 GB | 1       | 113   | 10    | 0.03   |
| Toshiba   | MQ03UBB300         | 3 TB   | 1       | 8     | 0     | 0.02   |
| Toshiba   | MK8025GAS          | 80 GB  | 1       | 5     | 0     | 0.01   |
| Toshiba   | MQ02ABD100H        | 1 TB   | 1       | 349   | 69    | 0.01   |
| Toshiba   | MK2018GAP          | 20 GB  | 1       | 126   | 55    | 0.01   |
| Toshiba   | MK1059GSM          | 1 TB   | 1       | 8     | 558   | 0.00   |

HDD by Family
-------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Family                 | Models | Samples | Days  | Err   | MTBF   |
|-----------|------------------------|--------|---------|-------|-------|--------|
| Toshiba   | 2.5" HDD MQ01ABD..H    | 1      | 1       | 1970  | 0     | 5.40   |
| Toshiba   | 3.5" MG03ACAxxx(Y) ... | 1      | 5       | 1631  | 0     | 4.47   |
| Toshiba   | 3.5" HDD DT01ACA       | 4      | 25      | 1289  | 42    | 3.23   |
| Toshiba   | 2.5" HDD MQ01UBB       | 1      | 1       | 1067  | 0     | 2.93   |
| Toshiba   | 3.5" MG04ACA Enterp... | 2      | 3       | 1033  | 0     | 2.83   |
| Toshiba   | X300                   | 1      | 6       | 1186  | 5     | 2.73   |
| Toshiba   | 2.5" HDD MQ04UBD       | 1      | 1       | 906   | 0     | 2.48   |
| Toshiba   | Unknown                | 2      | 3       | 809   | 0     | 2.22   |
| Toshiba   | 2.5" HDD MQ04UBF       | 1      | 1       | 592   | 0     | 1.62   |
| Toshiba   | 3.5" HDD N300          | 1      | 4       | 410   | 0     | 1.12   |
| Toshiba   | P300                   | 3      | 14      | 360   | 0     | 0.99   |
| Toshiba   | 2.5" HDD MK..59GSXP    | 1      | 1       | 326   | 0     | 0.89   |
| Toshiba   | 2.5" HDD MQ01ABD       | 1      | 5       | 286   | 143   | 0.52   |
| Toshiba   | 2.5" HDD MQ01ABF       | 1      | 3       | 195   | 1     | 0.29   |
| Toshiba   | 2.5" HDD MK..75GSX     | 1      | 1       | 604   | 5     | 0.28   |
| Toshiba   | N300                   | 1      | 3       | 87    | 0     | 0.24   |
| Toshiba   | 2.5" HDD MQ04ABF       | 1      | 6       | 85    | 1     | 0.18   |
| Toshiba   | 3.5" DT01ABA Deskto... | 1      | 1       | 232   | 8     | 0.07   |
| Toshiba   | 2.5" HDD MK..61GSY[N]  | 1      | 1       | 15    | 0     | 0.04   |
| Toshiba   | 2.5" HDD MK..65GSX     | 1      | 1       | 113   | 10    | 0.03   |
| Toshiba   | 2.5" HDD MQ03UBB       | 1      | 1       | 8     | 0     | 0.02   |
| Toshiba   | 2.5" HDD MQ02ABD..H    | 1      | 1       | 349   | 69    | 0.01   |
| Toshiba   | 2.5" HDD               | 2      | 2       | 65    | 28    | 0.01   |
| Toshiba   | 2.5" HDD MK..59GSM     | 1      | 1       | 8     | 558   | 0.00   |
