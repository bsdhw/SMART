Appendix 5: All NVMe Samples
============================

This is a list of all tested NVMe samples and their MTBFs. See more info on
reliability test in the README. See HDD samples MTBFs in the Appendix 1 (All_HDD.md)
and SSD samples MTBFs in the Appendix 2 (All_SSD.md).

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Drive ID     | Days  | Err   | MTBF   |
|-----------|--------------------|--------|--------------|-------|-------|--------|
| Samsung   | SSD 950 PRO        | 512 GB | 92A52D3EFDDF | 1264  | 0     | 3.46   |
| Samsung   | SSD 960 EVO        | 250 GB | C85B6A964080 | 1093  | 0     | 2.99   |
| Samsung   | SSD 950 PRO        | 512 GB | CF59E2A4C84B | 932   | 0     | 2.55   |
| Samsung   | MZVPV256HDGL-000L7 | 256 GB | 344D509D4D1D | 915   | 0     | 2.51   |
| Samsung   | SSD 960 EVO        | 250 GB | D79F04A150B2 | 653   | 0     | 1.79   |
| Samsung   | MZVLW256HEHP-000L7 | 256 GB | DC5575E5A3F5 | 637   | 0     | 1.75   |
| Plextor   | PX-256M8PeG        | 256 GB | B31EDA7FC3DC | 598   | 0     | 1.64   |
| HP        | SSD EX900          | 120 GB | 9FEA7A20C143 | 591   | 0     | 1.62   |
| HP        | SSD EX920          | 512 GB | B3F61D287734 | 531   | 0     | 1.46   |
| Patriot   | Scorch M2          | 128 GB | 4FFB7A828C8D | 493   | 0     | 1.35   |
| Samsung   | SSD 970 PRO        | 512 GB | C27F415DD01D | 466   | 0     | 1.28   |
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 7193D1AF750A | 456   | 0     | 1.25   |
| Samsung   | MZVLB512HAJQ-000L7 | 512 GB | 119CD72C31F2 | 450   | 0     | 1.23   |
| Intel     | SSDPEKNW010T8      | 1 TB   | 8AD553263F58 | 405   | 0     | 1.11   |
| Intel     | SSDPEKKF256G8L     | 256 GB | C605AC30C6EB | 376   | 0     | 1.03   |
| Toshiba   | KXG50ZNV512G       | 512 GB | DAC01454E2AB | 363   | 0     | 1.00   |
| Corsair   | Force MP600        | 2 TB   | B51DFBBF45D9 | 308   | 0     | 0.85   |
| Intel     | SSDPED1D480GA      | 480 GB | C94327B67965 | 299   | 0     | 0.82   |
| SPCC      | M.2 PCIe SSD       | 512 GB | 078491B25E92 | 297   | 0     | 0.81   |
| SPCC      | M.2 PCIe SSD       | 512 GB | 95983284E5E4 | 297   | 0     | 0.81   |
| Samsung   | SSD 960 EVO        | 500 GB | 7060974C9893 | 264   | 0     | 0.72   |
| WDC       | WDS500G1B0C-00S6U0 | 500 GB | 630F19148D3C | 257   | 0     | 0.71   |
| Toshiba   | THNSF5256GPUK      | 256 GB | 6791C952ED44 | 247   | 0     | 0.68   |
| Crucial   | CT1000P1SSD8       | 1 TB   | DDFC64A5DEA0 | 244   | 0     | 0.67   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | DDE2039D4196 | 239   | 0     | 0.66   |
| ADATA     | SX8200PNP          | 1 TB   | 720415F6EBAD | 228   | 0     | 0.63   |
| ADATA     | SX8200PNP          | 1 TB   | B527D04165EF | 227   | 0     | 0.62   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 0A950756FCED | 168   | 0     | 0.46   |
| Intel     | SSDPEDMW800G4      | 800 GB | 062A311A2DB5 | 168   | 0     | 0.46   |
| Toshiba   | RC100              | 240 GB | 0F2259F2C87A | 151   | 0     | 0.41   |
| ORICO     | V500               | 1 TB   | 66EA9B8A54A7 | 149   | 0     | 0.41   |
| Crucial   | CT1000P1SSD8       | 1 TB   | BE36737DD94F | 138   | 0     | 0.38   |
| WDC       | WDS250G2B0C-00PXH0 | 250 GB | 332EA212211F | 137   | 0     | 0.38   |
| Samsung   | MZVLB512HAJQ-000L7 | 512 GB | DF49555F9E69 | 137   | 0     | 0.38   |
| Toshiba   | THNSF5256GCJ7      | 256 GB | 1C100D40ACE4 | 130   | 0     | 0.36   |
| Samsung   | MZVLB512HBJQ-000L7 | 512 GB | ECFDE535B58D | 123   | 0     | 0.34   |
| SPCC      | M.2 PCIe SSD       | 1 TB   | 222119F710A0 | 121   | 0     | 0.33   |
| Samsung   | MZVLB512HAJQ-000L7 | 512 GB | 7B48EF5E0FC0 | 118   | 0     | 0.32   |
| Samsung   | PM961 NVMe         | 512 GB | 44845DB274F9 | 116   | 0     | 0.32   |
| Corsair   | Force MP510 1.9TB  | 1.9 TB | EAB4CDC80694 | 109   | 0     | 0.30   |
| Corsair   | Force MP300        | 120 GB | F4CBFF7DF93A | 102   | 0     | 0.28   |
| Corsair   | Force MP600        | 500 GB | 78E7FA9AEE5A | 91    | 0     | 0.25   |
| Samsung   | SSD 960 EVO        | 250 GB | A8FC59EFE86C | 89    | 0     | 0.25   |
| SK hynix  | BC501 HFM256GDJ... | 256 GB | D68143FA671F | 87    | 0     | 0.24   |
| Samsung   | SSD 970 PRO        | 1 TB   | 627FEE67D3D3 | 87    | 0     | 0.24   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 810CB3101241 | 81    | 0     | 0.22   |
| Intel     | HBRPEKNX0202AHO    | 32 GB  | 9BEBB80B2874 | 81    | 0     | 0.22   |
| Intel     | HBRPEKNX0202AH     | 512 GB | A706E4969CB4 | 80    | 0     | 0.22   |
| Samsung   | SSD 970 PRO        | 1 TB   | 4B5F8397940A | 79    | 0     | 0.22   |
| Crucial   | CT1000P1SSD8       | 1 TB   | 472165567C94 | 70    | 0     | 0.19   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 62D53A8D2B01 | 65    | 0     | 0.18   |
| Intel     | SSDPEKKF512G8L     | 512 GB | C41AEE0DEFF3 | 60    | 0     | 0.17   |
| Samsung   | MZVLB512HAJQ-000L7 | 512 GB | BFB138E8C58E | 302   | 4     | 0.17   |
| SPCC      | M.2 PCIe SSD       | 1 TB   | 72A92D62D1A5 | 57    | 0     | 0.16   |
| Crucial   | CT1000P1SSD8       | 1 TB   | 7F9391EAB2DD | 52    | 0     | 0.14   |
| Crucial   | CT1000P1SSD8       | 1 TB   | 856475B1B4C6 | 50    | 0     | 0.14   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 2C2089697122 | 47    | 0     | 0.13   |
| Intel     | SSDPEKNW512G8      | 512 GB | B10CE195512D | 46    | 0     | 0.13   |
| Samsung   | SSD 970 EVO        | 500 GB | 5487B2BA5B7D | 42    | 0     | 0.12   |
| Samsung   | PM981 NVMe         | 256 GB | 72876E86CDBE | 40    | 0     | 0.11   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | AAB333C4123C | 39    | 0     | 0.11   |
| Samsung   | SSD 960 EVO        | 1 TB   | 241D5CAD434E | 154   | 3     | 0.11   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | B8B03010F561 | 37    | 0     | 0.10   |
| Phison    | Sabrent Rocket 4.0 | 1 TB   | 957A773CD755 | 36    | 0     | 0.10   |
| Samsung   | MZVLB512HBJQ-000L7 | 512 GB | 472D1457DF58 | 35    | 0     | 0.10   |
| SK hynix  | BC501 HFM256GDJ... | 256 GB | 97566FC3F327 | 33    | 0     | 0.09   |
| WDC       | PC SN730 SDBQNT... | 512 GB | 10CED488822A | 32    | 0     | 0.09   |
| WDC       | PC SN730 SDBQNT... | 1 TB   | E1CE12D55333 | 32    | 0     | 0.09   |
| Kingston  | RBUSNS8154P3512GJ  | 512 GB | DCA288457306 | 25    | 0     | 0.07   |
| WDC       | PC SN730 SDBQNT... | 256 GB | EEE9C54F9D90 | 23    | 0     | 0.06   |
| SK hynix  | PC401 NVMe         | 256 GB | B33617E42C56 | 22    | 0     | 0.06   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 34BD777FE976 | 18    | 0     | 0.05   |
| Samsung   | MZVLB256HBHQ-000L7 | 256 GB | AEA8555AAEF0 | 17    | 0     | 0.05   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 16075A5AFEDD | 13    | 0     | 0.04   |
| Lite-On   | CA3-8D512          | 512 GB | 7E32956C3542 | 10    | 0     | 0.03   |
| Phison    | PCIe SSD           | 512 GB | FF2AEE215087 | 10    | 0     | 0.03   |
| Micron    | 2200V_MTFDHBA51... | 512 GB | 382F0B471B48 | 7     | 0     | 0.02   |
| Union ... | UMIS LENSE40512... | 512 GB | 2362C71656A4 | 6     | 0     | 0.02   |
| Samsung   | MZVLQ256HAJD-00000 | 256 GB | 0DEE00114F04 | 5     | 0     | 0.02   |
| Samsung   | MZVLW512HMJP-00000 | 512 GB | B0710EFBD2C9 | 5     | 0     | 0.02   |
| WDC       | PC SN520 SDAPNU... | 512 GB | 3C6B9F78D710 | 5     | 0     | 0.01   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | B1EC5A1ACC12 | 2     | 0     | 0.01   |
| Toshiba   | KBG30ZMT512G       | 512 GB | 12D22891F0CB | 2     | 0     | 0.01   |
| Union ... | RPFTJ128PDD2EWX    | 128 GB | CFB305F61781 | 1     | 0     | 0.00   |
| SK hynix  | HFM256GDJTNG-8310A | 256 GB | B7E910BF6EA5 | 1     | 0     | 0.00   |
| Intel     | SSDPEKKF512G8L     | 512 GB | BFC3C6089EF9 | 1     | 0     | 0.00   |
| Toshiba   | KXG60PNV2T04 NV... | 2 TB   | EE4D4287C626 | 0     | 0     | 0.00   |
| ADATA     | SX6000LNP          | 512 GB | 45B6FD12C14D | 0     | 0     | 0.00   |
| SK hynix  | SKHynix_HFS512G... | 512 GB | B16F71D07239 | 0     | 0     | 0.00   |
