Appendix 4: Top 1000 SSD Models
===============================

See more info on reliability test in the [README](https://github.com/bsdhw/SMART).

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Samsung   | SSD 840 EVO        | 120 GB | 2       | 3427  | 0     | 9.39   |
| Intel     | SSDSA2M040G2GC     | 40 GB  | 1       | 2739  | 0     | 7.51   |
| WDC       | WDBNCE5000PNC      | 500 GB | 1       | 2730  | 0     | 7.48   |
| Samsung   | SSD 840 Series     | 120 GB | 1       | 2655  | 0     | 7.28   |
| Samsung   | SSD 840 PRO Series | 128 GB | 1       | 2487  | 0     | 6.81   |
| OCZ       | AGILITY3           | 120 GB | 1       | 2119  | 0     | 5.81   |
| Samsung   | MZ7TD256HAFV-000L7 | 256 GB | 1       | 2052  | 0     | 5.62   |
| Intel     | SSDSC2CT180A3      | 180 GB | 2       | 2012  | 0     | 5.51   |
| Intel     | SSDSC2CT080A4      | 80 GB  | 1       | 1964  | 0     | 5.38   |
| Intel     | SSDSC2BF120A5      | 120 GB | 1       | 1960  | 0     | 5.37   |
| Intel     | SSDSA2CT040G3      | 40 GB  | 2       | 1958  | 0     | 5.37   |
| OCZ       | VERTEX3            | 64 GB  | 2       | 1948  | 0     | 5.34   |
| ADATA     | SP310              | 32 GB  | 1       | 1944  | 0     | 5.33   |
| Samsung   | SSD PB22-JS3 TM    | 64 GB  | 1       | 1801  | 0     | 4.94   |
| Samsung   | SSD 850 PRO        | 128 GB | 2       | 1798  | 0     | 4.93   |
| Kingston  | SH103S3240G        | 240 GB | 1       | 1798  | 0     | 4.93   |
| Kingston  | SMS200S360G        | 64 GB  | 2       | 1773  | 0     | 4.86   |
| OCZ       | VERTEX3            | 120 GB | 1       | 1657  | 0     | 4.54   |
| Micron    | M600_MTFDDAK256MBF | 256 GB | 1       | 1656  | 0     | 4.54   |
| Samsung   | MZMTE512HMHP-000MV | 512 GB | 2       | 1593  | 0     | 4.36   |
| ADATA     | SP900              | 128 GB | 1       | 1548  | 0     | 4.24   |
| SK hynix  | HFS256G3AMNB-2200A | 256 GB | 1       | 1542  | 0     | 4.22   |
| Goodram   | SSD                | 240 GB | 2       | 1509  | 0     | 4.13   |
| ADATA     | SP310              | 128 GB | 1       | 1488  | 0     | 4.08   |
| SanDisk   | SSD U100           | 16 GB  | 1       | 1440  | 0     | 3.95   |
| Intel     | SSDSC2BW240A3L     | 240 GB | 1       | 1425  | 0     | 3.91   |
| Samsung   | SSD 750 EVO        | 120 GB | 1       | 1425  | 0     | 3.91   |
| Samsung   | SSD 840 Series     | 250 GB | 2       | 1328  | 0     | 3.64   |
| Samsung   | MZ7LM240HCGR-0E003 | 240 GB | 2       | 1295  | 0     | 3.55   |
| WDC       | WDS120G1G0A-00SS50 | 120 GB | 1       | 1219  | 0     | 3.34   |
| SanDisk   | Ultra II           | 960 GB | 2       | 1202  | 0     | 3.29   |
| Samsung   | SSD 850 EVO M.2    | 500 GB | 3       | 1191  | 0     | 3.26   |
| Toshiba   | THNSNJ128GCSU      | 128 GB | 2       | 1189  | 0     | 3.26   |
| Samsung   | SSD 840 PRO Series | 256 GB | 2       | 1183  | 0     | 3.24   |
| Samsung   | SSD RBX Series ... | 64 GB  | 1       | 1182  | 0     | 3.24   |
| SuperM... | SSD                | 16 GB  | 1       | 1176  | 0     | 3.22   |
| Samsung   | SSD 850 EVO mSATA  | 1 TB   | 1       | 2157  | 1     | 2.96   |
| SanDisk   | SD7SF6S512G1122    | 512 GB | 1       | 1073  | 0     | 2.94   |
| Transcend | TS64GSSD25S-M      | 64 GB  | 1       | 1067  | 0     | 2.92   |
| Samsung   | MZ7PD128HAFV-000H7 | 128 GB | 1       | 1060  | 0     | 2.91   |
| Intel     | SSDSC2BB080G4      | 80 GB  | 1       | 1060  | 0     | 2.91   |
| Kingston  | SKC400S37512G      | 512 GB | 2       | 1059  | 0     | 2.90   |
| Crucial   | FCCT128M4SSD1      | 128 GB | 2       | 1042  | 0     | 2.86   |
| Samsung   | SSD 850 EVO mSATA  | 250 GB | 3       | 1033  | 0     | 2.83   |
| ADATA     | SX930              | 240 GB | 1       | 1001  | 0     | 2.74   |
| Crucial   | CT250MX200SSD1     | 250 GB | 5       | 1048  | 1     | 2.68   |
| Kingston  | SH103S3120G        | 120 GB | 2       | 964   | 0     | 2.64   |
| Intel     | SSDSC2BB016T4      | 1.6 TB | 1       | 959   | 0     | 2.63   |
| China     | SATA SSD           | 120 GB | 1       | 933   | 0     | 2.56   |
| Crucial   | CT240M500SSD3      | 240 GB | 1       | 917   | 0     | 2.51   |
| SanDisk   | SD6SB1M064G1022I   | 64 GB  | 2       | 898   | 0     | 2.46   |
| Seagate   | ST100FN0021        | 100 GB | 1       | 1781  | 1     | 2.44   |
| SanDisk   | SD8SN8U-256G-1006  | 256 GB | 1       | 888   | 0     | 2.44   |
| Transcend | TS128GSSD370S      | 128 GB | 1       | 868   | 0     | 2.38   |
| Samsung   | SSD 750 EVO        | 500 GB | 1       | 854   | 0     | 2.34   |
| SanDisk   | SD8SN8U128G1122    | 128 GB | 2       | 848   | 0     | 2.33   |
| Corsair   | Force 3 SSD        | 180 GB | 1       | 2530  | 2     | 2.31   |
| OCZ       | VERTEX2            | 120 GB | 1       | 835   | 0     | 2.29   |
| Samsung   | SSD 850 EVO        | 250 GB | 18      | 835   | 0     | 2.29   |
| Intel     | SSDSC2BP240G4      | 240 GB | 1       | 835   | 0     | 2.29   |
| Toshiba   | THNSNC128GBSJ      | 128 GB | 1       | 826   | 0     | 2.26   |
| Kingston  | SUV400S37240G      | 240 GB | 3       | 815   | 0     | 2.23   |
| Kingston  | SUV300S37A240G     | 240 GB | 1       | 807   | 0     | 2.21   |
| Crucial   | CT1050MX300SSD1    | 1 TB   | 3       | 804   | 0     | 2.20   |
| Micron    | 1100_MTFDDAK512TBN | 512 GB | 2       | 798   | 0     | 2.19   |
| SanDisk   | SSD U110           | 16 GB  | 3       | 797   | 0     | 2.19   |
| Micron    | 5100_MTFDDAK240TCB | 240 GB | 4       | 778   | 0     | 2.13   |
| MyDigi... | SB2                | 128 GB | 1       | 763   | 0     | 2.09   |
| Samsung   | SSD 850 EVO        | 1 TB   | 5       | 1085  | 1     | 2.09   |
| Apple     | SSD TS256C         | 256 GB | 1       | 752   | 0     | 2.06   |
| Samsung   | SSD 850 EVO        | 2 TB   | 1       | 742   | 0     | 2.03   |
| Mushkin   | MKNSSDTR1TB-3DL    | 1 TB   | 1       | 734   | 0     | 2.01   |
| Samsung   | SSD 750 EVO        | 250 GB | 1       | 734   | 0     | 2.01   |
| Samsung   | SSD 840 EVO        | 1 TB   | 1       | 727   | 0     | 1.99   |
| Intel     | SSDSC2BW240H6      | 240 GB | 2       | 723   | 0     | 1.98   |
| Samsung   | MZMPC032HBCD-00000 | 32 GB  | 1       | 721   | 0     | 1.98   |
| Samsung   | SSD 860 EVO        | 2 TB   | 2       | 717   | 0     | 1.97   |
| Samsung   | SSD 840 EVO        | 250 GB | 3       | 690   | 0     | 1.89   |
| Samsung   | MZ7PC128HAFU-000L1 | 128 GB | 1       | 683   | 0     | 1.87   |
| OWC       | Mercury Extreme... | 240 GB | 1       | 657   | 0     | 1.80   |
| WDC       | WDS250G1B0A-00H9H0 | 250 GB | 1       | 651   | 0     | 1.78   |
| Kingston  | SMSM150S324G2      | 24 GB  | 1       | 650   | 0     | 1.78   |
| SanDisk   | Ultra II           | 480 GB | 2       | 622   | 0     | 1.70   |
| SPCC      | SSD                | 1 TB   | 1       | 617   | 0     | 1.69   |
| SanDisk   | SDSSDHII120G       | 120 GB | 1       | 613   | 0     | 1.68   |
| Kingston  | SUV500MS120G       | 120 GB | 3       | 611   | 0     | 1.68   |
| Intel     | SSDSA2CW160G3      | 160 GB | 1       | 610   | 0     | 1.67   |
| SanDisk   | SDSSDXP120G        | 120 GB | 1       | 608   | 0     | 1.67   |
| Samsung   | SSD 850 PRO        | 256 GB | 5       | 608   | 0     | 1.67   |
| Samsung   | MZNLN256HCHP-000L7 | 256 GB | 1       | 595   | 0     | 1.63   |
| Crucial   | M4-CT064M4SSD2     | 64 GB  | 1       | 589   | 0     | 1.61   |
| SanDisk   | SDSSDA120G         | 120 GB | 2       | 566   | 0     | 1.55   |
| Samsung   | SSD 850 EVO        | 500 GB | 10      | 550   | 0     | 1.51   |
| Samsung   | MZ7TE256HMHP-000H1 | 256 GB | 1       | 544   | 0     | 1.49   |
| Phison    | SATA SSD           | 16 GB  | 9       | 533   | 0     | 1.46   |
| Samsung   | MZ7TY256HDHP-000L7 | 256 GB | 1       | 516   | 0     | 1.41   |
| Kingston  | SV300S37A480G      | 480 GB | 1       | 501   | 0     | 1.37   |
| Micron    | 1100_MTFDDAK256TBN | 256 GB | 3       | 496   | 0     | 1.36   |
| Plextor   | PX-128M7VC         | 128 GB | 1       | 483   | 0     | 1.32   |
| Samsung   | MZ7LN512HMJP-000L7 | 512 GB | 1       | 483   | 0     | 1.32   |
| Patriot   | Burst              | 240 GB | 1       | 467   | 0     | 1.28   |
| AEGO      | SSD                | 120 GB | 1       | 445   | 0     | 1.22   |
| Samsung   | SSD PM830 FDE 2... | 256 GB | 1       | 440   | 0     | 1.21   |
| Vaseky    | V800-60G           | 64 GB  | 1       | 426   | 0     | 1.17   |
| Samsung   | SSD 860 EVO M.2    | 2 TB   | 2       | 425   | 0     | 1.17   |
| SPCC      | SSD                | 64 GB  | 2       | 424   | 0     | 1.16   |
| SanDisk   | SD8SN8U128G1002    | 128 GB | 1       | 424   | 0     | 1.16   |
| Toshiba   | THNSNJ128GCST      | 128 GB | 1       | 422   | 0     | 1.16   |
| SanDisk   | SDSSDH3512G        | 512 GB | 1       | 415   | 0     | 1.14   |
| Intel     | SSDSC2BB120G4      | 120 GB | 1       | 413   | 0     | 1.13   |
| Transcend | TS512GSSD370       | 512 GB | 1       | 412   | 0     | 1.13   |
| Crucial   | CT525MX300SSD1     | 528 GB | 5       | 512   | 1     | 1.08   |
| Samsung   | SSD PM830 mSATA    | 128 GB | 1       | 385   | 0     | 1.06   |
| Kingston  | SVP200S37A60G      | 64 GB  | 1       | 377   | 0     | 1.03   |
| Hoodisk   | SSD                | 32 GB  | 3       | 372   | 0     | 1.02   |
| Crucial   | CT120M500SSD1      | 120 GB | 2       | 369   | 0     | 1.01   |
| Kingston  | RBUSNS8180S3128GI  | 128 GB | 1       | 351   | 0     | 0.96   |
| Intel     | SSDSC2BB480G7      | 480 GB | 1       | 703   | 1     | 0.96   |
| Samsung   | SSD 860 EVO mSATA  | 1 TB   | 1       | 346   | 0     | 0.95   |
| Intel     | SSDSA2M080G2GC     | 80 GB  | 1       | 3057  | 8     | 0.93   |
| Kingston  | SHFS37A120G        | 120 GB | 1       | 334   | 0     | 0.92   |
| Intel     | SSDSA2CW120G3      | 120 GB | 1       | 326   | 0     | 0.89   |
| Samsung   | SSD 860 EVO        | 1 TB   | 6       | 320   | 0     | 0.88   |
| Phison    | SATA SSD           | 128 GB | 1       | 316   | 0     | 0.87   |
| SanDisk   | SSD PLUS           | 120 GB | 8       | 312   | 0     | 0.86   |
| Kingston  | SA400S37240G       | 240 GB | 12      | 308   | 0     | 0.85   |
| Toshiba   | THNSFJ256GCSU      | 256 GB | 2       | 308   | 0     | 0.85   |
| Kingston  | SUV500MS240G       | 240 GB | 4       | 307   | 0     | 0.84   |
| Samsung   | SSD 850 PRO        | 512 GB | 1       | 306   | 0     | 0.84   |
| KingDian  | S400               | 120 GB | 1       | 303   | 0     | 0.83   |
| OCZ       | VERTEX4            | 128 GB | 1       | 1188  | 3     | 0.81   |
| Intel     | SSDSC2CT240A3      | 240 GB | 1       | 2370  | 7     | 0.81   |
| Kingston  | SV300S37A120G      | 120 GB | 4       | 410   | 1     | 0.77   |
| Samsung   | SSD 860 PRO        | 256 GB | 1       | 279   | 0     | 0.76   |
| Kingston  | SMS200S3120G       | 120 GB | 3       | 881   | 3     | 0.76   |
| SanDisk   | SD9SN8W128G1002    | 128 GB | 1       | 278   | 0     | 0.76   |
| Phison    | SATA SSD           | 120 GB | 1       | 273   | 0     | 0.75   |
| Kingston  | SA400S37120G       | 120 GB | 7       | 271   | 0     | 0.74   |
| SanDisk   | SD6SB2M512G1022I   | 512 GB | 1       | 536   | 1     | 0.74   |
| SK hynix  | HFS128G32MND-2200A | 128 GB | 1       | 798   | 2     | 0.73   |
| Intel     | SSDSC2BF180A4L     | 180 GB | 3       | 392   | 2     | 0.72   |
| Samsung   | SSD 850 EVO        | 120 GB | 2       | 255   | 0     | 0.70   |
| SanDisk   | SD5SG2256G1052E    | 256 GB | 1       | 246   | 0     | 0.68   |
| Samsung   | SSD 860 EVO        | 500 GB | 9       | 244   | 0     | 0.67   |
| Intel     | SSDSA2M160G2GC     | 160 GB | 1       | 472   | 1     | 0.65   |
| SK hynix  | HFS128G39TND-N210A | 128 GB | 1       | 232   | 0     | 0.64   |
| Plextor   | PX-256M8VG         | 256 GB | 1       | 230   | 0     | 0.63   |
| SPCC      | SSD                | 120 GB | 1       | 228   | 0     | 0.63   |
| WDC       | WDS500G2B0A-00SM50 | 500 GB | 1       | 223   | 0     | 0.61   |
| Samsung   | MZHPV512HDGL-000L1 | 512 GB | 1       | 222   | 0     | 0.61   |
| Kingston  | SMS200S330G        | 32 GB  | 2       | 768   | 2     | 0.60   |
| Corsair   | Force LS SSD       | 64 GB  | 2       | 217   | 0     | 0.60   |
| Crucial   | CT275MX300SSD4     | 275 GB | 1       | 213   | 0     | 0.59   |
| OCZ       | VERTEX4            | 64 GB  | 1       | 206   | 0     | 0.57   |
| Samsung   | MZ7LN256HCHP-000L7 | 256 GB | 1       | 204   | 0     | 0.56   |
| SanDisk   | X400 M.2 2280      | 512 GB | 1       | 204   | 0     | 0.56   |
| Samsung   | SSD 860 EVO mSATA  | 500 GB | 6       | 203   | 0     | 0.56   |
| Crucial   | CT250MX500SSD1     | 250 GB | 7       | 203   | 0     | 0.56   |
| Crucial   | CT500MX200SSD1     | 500 GB | 2       | 199   | 0     | 0.55   |
| Kingston  | SUV500240G         | 240 GB | 2       | 198   | 0     | 0.55   |
| WDC       | WDS500G1B0A-00H9H0 | 500 GB | 1       | 196   | 0     | 0.54   |
| PNY       | CS900 120GB SSD    | 120 GB | 4       | 189   | 0     | 0.52   |
| Apacer    | 128GB SATA Flas... | 128 GB | 1       | 187   | 0     | 0.51   |
| China     | SSD 128G           | 128 GB | 1       | 181   | 0     | 0.50   |
| Kingston  | SM2280S3G2240G     | 240 GB | 1       | 181   | 0     | 0.50   |
| Hoodisk   | SSD                | 64 GB  | 2       | 180   | 0     | 0.50   |
| KingDian  | S280-240GB         | 240 GB | 1       | 177   | 0     | 0.49   |
| Goodram   | SSD                | 120 GB | 1       | 172   | 0     | 0.47   |
| Apple     | SSD SM0512G        | 500 GB | 1       | 170   | 0     | 0.47   |
| WDC       | WDS240G2G0B-00EPW0 | 240 GB | 2       | 168   | 0     | 0.46   |
| ZTC       | SM201-064G         | 64 GB  | 1       | 164   | 0     | 0.45   |
| Vaseky    | 128GV800           | 128 GB | 1       | 161   | 0     | 0.44   |
| Apacer    | 32GB SATA Flash... | 32 GB  | 1       | 1769  | 10    | 0.44   |
| Samsung   | SSD PM871b M.2 ... | 256 GB | 1       | 158   | 0     | 0.43   |
| China     | 40GB SATA Flash... | 40 GB  | 1       | 156   | 0     | 0.43   |
| SK hynix  | SC311 SATA         | 512 GB | 1       | 153   | 0     | 0.42   |
| Intel     | SSDSCKGF180A4L     | 180 GB | 1       | 152   | 0     | 0.42   |
| Transcend | TS256GSSD230S      | 256 GB | 1       | 147   | 0     | 0.40   |
| Phison    | SATA SSD           | 32 GB  | 1       | 145   | 0     | 0.40   |
| Samsung   | SSD 860 EVO M.2    | 500 GB | 1       | 144   | 0     | 0.40   |
| Lite-On   | CV8-8E256-11 SATA  | 256 GB | 1       | 143   | 0     | 0.39   |
| Samsung   | SSD 860 EVO        | 250 GB | 7       | 143   | 0     | 0.39   |
| KingSpec  | NT-128             | 128 GB | 1       | 140   | 0     | 0.38   |
| SanDisk   | SD6SB1M128G1022I   | 128 GB | 1       | 139   | 0     | 0.38   |
| Intel     | SSDSC2CW060A3      | 64 GB  | 2       | 1461  | 509   | 0.38   |
| Samsung   | SSD 860 QVO        | 4 TB   | 2       | 135   | 0     | 0.37   |
| Toshiba   | TR200              | 240 GB | 2       | 131   | 0     | 0.36   |
| Samsung   | MZHPV256HDGL-000L1 | 256 GB | 1       | 131   | 0     | 0.36   |
| SanDisk   | SD7SN3Q128G1002    | 128 GB | 1       | 131   | 0     | 0.36   |
| Samsung   | SSD 850 EVO M.2    | 250 GB | 2       | 130   | 0     | 0.36   |
| WDC       | WDS500G2B0A        | 500 GB | 2       | 125   | 0     | 0.34   |
| Lite-On   | LCS-128M6S-HP      | 128 GB | 1       | 123   | 0     | 0.34   |
| Kingston  | SA400S37480G       | 480 GB | 3       | 119   | 0     | 0.33   |
| SanDisk   | SDSSDH3 512G       | 512 GB | 1       | 116   | 0     | 0.32   |
| Verbatim  | Vi500 S3 480GB SSD | 480 GB | 1       | 115   | 0     | 0.32   |
| Samsung   | SSD 860 QVO        | 1 TB   | 3       | 113   | 0     | 0.31   |
| Crucial   | CT120BX300SSD1     | 120 GB | 1       | 108   | 0     | 0.30   |
| China     | OSSD256GBTSS2      | 256 GB | 1       | 107   | 0     | 0.30   |
| China     | BR                 | 64 GB  | 1       | 106   | 0     | 0.29   |
| Intel     | SSDSC2KG480G8R     | 480 GB | 2       | 105   | 0     | 0.29   |
| Patriot   | Burst              | 120 GB | 1       | 104   | 0     | 0.28   |
| SanDisk   | SD9SN8W128G1102    | 128 GB | 1       | 98    | 0     | 0.27   |
| Apple     | SSD SD0128F        | 121 GB | 1       | 91    | 0     | 0.25   |
| Apple     | SSD SM0128G        | 121 GB | 2       | 91    | 0     | 0.25   |
| Crucial   | CT1000MX500SSD1    | 1 TB   | 3       | 89    | 0     | 0.24   |
| Intel     | SSDSC2BW240A4      | 240 GB | 1       | 89    | 0     | 0.24   |
| Samsung   | SSD 860 QVO        | 2 TB   | 1       | 88    | 0     | 0.24   |
| OCZ       | AGILITY4           | 128 GB | 1       | 176   | 1     | 0.24   |
| ADATA     | SU800              | 256 GB | 1       | 85    | 0     | 0.24   |
| ADATA     | SP600              | 64 GB  | 1       | 84    | 0     | 0.23   |
| ADATA     | SU800              | 1 TB   | 1       | 83    | 0     | 0.23   |
| WDC       | WDS120G2G0A-00JH30 | 120 GB | 1       | 82    | 0     | 0.23   |
| KingDian  | S200               | 64 GB  | 2       | 80    | 0     | 0.22   |
| Crucial   | CT500MX500SSD1     | 500 GB | 1       | 77    | 0     | 0.21   |
| Transcend | TS240GMTS420S      | 240 GB | 2       | 77    | 0     | 0.21   |
| Kingston  | SMSM151S3128GD     | 128 GB | 1       | 598   | 7     | 0.21   |
| SPCC      | SSD                | 256 GB | 1       | 71    | 0     | 0.19   |
| Intel     | SSDSC2KG480G8      | 480 GB | 3       | 70    | 0     | 0.19   |
| Samsung   | SSD 860 EVO        | 4 TB   | 1       | 68    | 0     | 0.19   |
| Plextor   | PX-256M5S          | 256 GB | 2       | 64    | 0     | 0.18   |
| WDC       | WDS480G2G0A-00JH30 | 480 GB | 3       | 60    | 0     | 0.16   |
| Samsung   | MZMTE1T0HMJH-00000 | 1 TB   | 1       | 59    | 0     | 0.16   |
| SanDisk   | Ultra II           | 240 GB | 1       | 58    | 0     | 0.16   |
| Kingston  | SUV500MS480G       | 480 GB | 2       | 49    | 0     | 0.14   |
| KingDian  | S280               | 120 GB | 1       | 49    | 0     | 0.14   |
| ADATA     | SU630              | 480 GB | 1       | 48    | 0     | 0.13   |
| Transcend | TS64GMSA370        | 64 GB  | 2       | 47    | 0     | 0.13   |
| Crucial   | CT250MX500SSD4     | 250 GB | 1       | 47    | 0     | 0.13   |
| OCZ       | TRION150           | 240 GB | 1       | 40    | 0     | 0.11   |
| WDC       | WDS240G2G0A-00JH30 | 240 GB | 8       | 45    | 1     | 0.11   |
| SanDisk   | SD6SB1M256G1022I   | 256 GB | 2       | 1754  | 44    | 0.11   |
| Crucial   | CT120BX500SSD1     | 120 GB | 2       | 39    | 0     | 0.11   |
| WDC       | WDS120G1G0B-00RC30 | 120 GB | 1       | 37    | 0     | 0.10   |
| KingFast  | SSD                | 120 GB | 2       | 37    | 0     | 0.10   |
| SanDisk   | SSD PLUS           | 1 TB   | 1       | 37    | 0     | 0.10   |
| Kingston  | SUV400S37 120G     | 120 GB | 1       | 37    | 0     | 0.10   |
| Micron    | 1100 SATA          | 512 GB | 1       | 36    | 0     | 0.10   |
| ADATA     | SU750              | 256 GB | 1       | 36    | 0     | 0.10   |
| Transcend | TS32GMSA370        | 32 GB  | 3       | 35    | 0     | 0.10   |
| Phison    | SATA SSD           | 64 GB  | 1       | 33    | 0     | 0.09   |
| OCZ       | VERTEX-TURBO       | 32 GB  | 2       | 298   | 56    | 0.09   |
| Apacer    | AS350              | 256 GB | 1       | 30    | 0     | 0.08   |
| Crucial   | CT480M500SSD1      | 480 GB | 1       | 580   | 20    | 0.08   |
| WDC       | WDS120G2G0B-00EPW0 | 120 GB | 1       | 26    | 0     | 0.07   |
| Kingston  | SHSS37A240G        | 240 GB | 1       | 25    | 0     | 0.07   |
| Crucial   | CT240BX500SSD1     | 240 GB | 2       | 23    | 0     | 0.06   |
| KingFast  | SSD                | 512 GB | 1       | 23    | 0     | 0.06   |
| China     | CS2246-M512        | 506 GB | 1       | 22    | 0     | 0.06   |
| SanDisk   | SSD PLUS           | 240 GB | 2       | 43    | 1     | 0.06   |
| Transcend | TS512GMSA370       | 512 GB | 1       | 21    | 0     | 0.06   |
| Micron    | MTFDDAV256MBF-1... | 256 GB | 1       | 21    | 0     | 0.06   |
| SanDisk   | SSD i100           | 64 GB  | 1       | 21    | 0     | 0.06   |
| China     | SATA SSD           | 256 GB | 1       | 21    | 0     | 0.06   |
| ORICO     | PH100-128G         | 128 GB | 1       | 19    | 0     | 0.05   |
| ADATA     | SU650              | 120 GB | 2       | 406   | 106   | 0.05   |
| Samsung   | MZNLN256HAJQ-000L7 | 256 GB | 1       | 14    | 0     | 0.04   |
| Transcend | TS256GMTS400       | 256 GB | 1       | 14    | 0     | 0.04   |
| Intel     | SSDSC2BF180A5H REF | 180 GB | 1       | 13    | 0     | 0.04   |
| Samsung   | SSD 860 EVO M.2    | 1 TB   | 1       | 13    | 0     | 0.04   |
| PNY       | SSD2SC240G5LC70... | 240 GB | 1       | 13    | 0     | 0.04   |
| KingSpec  | NT-512             | 512 GB | 1       | 12    | 0     | 0.04   |
| Lite-On   | LCH-256V2S         | 256 GB | 1       | 12    | 0     | 0.03   |
| Transcend | TS128GMSA370       | 128 GB | 1       | 11    | 0     | 0.03   |
| Intenso   | SSD Sata III       | 240 GB | 1       | 10    | 0     | 0.03   |
| Dogfish   | SSD                | 64 GB  | 1       | 10    | 0     | 0.03   |
| Crucial   | CT2000MX500SSD1    | 2 TB   | 1       | 10    | 0     | 0.03   |
| Leven     | JAJS600M512C       | 512 GB | 1       | 9     | 0     | 0.03   |
| Transcend | TS128GMTS430S      | 128 GB | 1       | 8     | 0     | 0.02   |
| WDC       | WDS100T2B0A-00SM50 | 1 TB   | 26      | 8     | 0     | 0.02   |
| Intenso   | SSD Sata III       | 128 GB | 2       | 7     | 26    | 0.02   |
| Micron    | 1100 SATA          | 256 GB | 1       | 22    | 2     | 0.02   |
| Apple     | SSD SM256E         | 256 GB | 1       | 809   | 122   | 0.02   |
| Lexar     | 256GB SSD          | 256 GB | 1       | 6     | 0     | 0.02   |
| Crucial   | M4-CT128M4SSD3     | 128 GB | 1       | 4     | 0     | 0.01   |
| Apacer    | AS350              | 128 GB | 1       | 4     | 0     | 0.01   |
| Team      | T253X1240G         | 240 GB | 1       | 4     | 0     | 0.01   |
| Kingston  | SHFS37A240G        | 240 GB | 1       | 4     | 0     | 0.01   |
| Mushkin   | MKNSSDSR120GB      | 120 GB | 1       | 3     | 0     | 0.01   |
| Innodisk  | 2.5" SATA SSD 3... | 128 GB | 1       | 3     | 0     | 0.01   |
| OWC       | Aura               | 960 GB | 1       | 3     | 0     | 0.01   |
| SK hynix  | SC210 mSATA        | 256 GB | 1       | 391   | 139   | 0.01   |
| Transcend | TS120GMTS420S      | 120 GB | 2       | 2     | 0     | 0.01   |
| Kingston  | SV300S37A60G       | 64 GB  | 1       | 1586  | 804   | 0.01   |
| Samsung   | MZNLH512HALU-00000 | 512 GB | 1       | 1     | 0     | 0.01   |
| Intel     | SSDSC2BA200G3T     | 200 GB | 2       | 1873  | 1028  | 0.00   |
| Intel     | SSDSCKKF256G8H     | 256 GB | 1       | 144   | 89    | 0.00   |
| Samsung   | MZHPV512HDGL-00000 | 512 GB | 1       | 1     | 0     | 0.00   |
| Samsung   | SSD UM410 Serie... | 16 GB  | 1       | 38    | 24    | 0.00   |
| China     | MSATA 64GB SSD     | 64 GB  | 1       | 1     | 0     | 0.00   |
| Kingston  | SNS4151S316G       | 16 GB  | 1       | 1252  | 1022  | 0.00   |
| Intel     | SSDSC2CW120A3      | 120 GB | 1       | 1235  | 1019  | 0.00   |
| Lite-On   | LCH-128V2S         | 128 GB | 1       | 1     | 0     | 0.00   |
| SanDisk   | SDSSDH3 250G       | 250 GB | 1       | 0     | 0     | 0.00   |
| SanDisk   | SD7UB3Q256G1001    | 256 GB | 1       | 84    | 100   | 0.00   |
| Kingston  | RBU-SNS4151S316GG2 | 16 GB  | 1       | 0     | 0     | 0.00   |
| FORESEE   | 64GB SSD           | 64 GB  | 1       | 0     | 0     | 0.00   |
| QUMO      | SSD                | 120 GB | 1       | 0     | 0     | 0.00   |
| SanDisk   | SSD G5 BICS4       | 500 GB | 1       | 0     | 0     | 0.00   |
| Zheino    | CHN-mSATAQ3-120    | 120 GB | 1       | 0     | 0     | 0.00   |
| Intel     | SSDSC2KW120H6      | 120 GB | 1       | 2     | 29    | 0.00   |
| Kingston  | SNS4151S332G       | 32 GB  | 1       | 79    | 1022  | 0.00   |
| Micron    | MTFDDAK256MAM-1K12 | 256 GB | 1       | 52    | 1010  | 0.00   |
| Intel     | SSDSC2KW480H6      | 480 GB | 1       | 39    | 1010  | 0.00   |
| Micron    | MTFDDAT128MAM-1J2  | 128 GB | 1       | 28    | 1587  | 0.00   |
