Toshiba NVMe Drives
===================

This is a list of all tested Toshiba NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Toshiba   | KXG50ZNV512G       | 512 GB | 1       | 363   | 0     | 1.00   |
| Toshiba   | THNSF5256GPUK      | 256 GB | 1       | 247   | 0     | 0.68   |
| Toshiba   | RC100              | 240 GB | 1       | 151   | 0     | 0.41   |
| Toshiba   | THNSF5256GCJ7      | 256 GB | 1       | 130   | 0     | 0.36   |
| Toshiba   | KBG30ZMT512G       | 512 GB | 1       | 2     | 0     | 0.01   |
| Toshiba   | KXG60PNV2T04 NV... | 2 TB   | 1       | 0     | 0     | 0.00   |
