Intel NVMe Drives
=================

This is a list of all tested Intel NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Intel     | SSDPEKNW010T8      | 1 TB   | 1       | 405   | 0     | 1.11   |
| Intel     | SSDPEKKF256G8L     | 256 GB | 1       | 376   | 0     | 1.03   |
| Intel     | SSDPED1D480GA      | 480 GB | 1       | 299   | 0     | 0.82   |
| Intel     | SSDPEDMW800G4      | 800 GB | 1       | 168   | 0     | 0.46   |
| Intel     | HBRPEKNX0202AHO    | 32 GB  | 1       | 81    | 0     | 0.22   |
| Intel     | HBRPEKNX0202AH     | 512 GB | 1       | 80    | 0     | 0.22   |
| Intel     | SSDPEKNW512G8      | 512 GB | 1       | 46    | 0     | 0.13   |
| Intel     | SSDPEKKF512G8L     | 512 GB | 2       | 30    | 0     | 0.08   |
