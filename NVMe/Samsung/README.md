Samsung NVMe Drives
===================

This is a list of all tested Samsung NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| Samsung   | SSD 950 PRO        | 512 GB | 2       | 1098  | 0     | 3.01   |
| Samsung   | MZVPV256HDGL-000L7 | 256 GB | 1       | 915   | 0     | 2.51   |
| Samsung   | MZVLW256HEHP-000L7 | 256 GB | 1       | 637   | 0     | 1.75   |
| Samsung   | SSD 960 EVO        | 250 GB | 3       | 612   | 0     | 1.68   |
| Samsung   | SSD 970 PRO        | 512 GB | 1       | 466   | 0     | 1.28   |
| Samsung   | SSD 960 EVO        | 500 GB | 1       | 264   | 0     | 0.72   |
| Samsung   | MZVLB256HBHQ-00000 | 256 GB | 1       | 239   | 0     | 0.66   |
| Samsung   | MZVLB512HAJQ-000L7 | 512 GB | 4       | 252   | 1     | 0.53   |
| Samsung   | PM961 NVMe         | 512 GB | 1       | 116   | 0     | 0.32   |
| Samsung   | SSD 970 PRO        | 1 TB   | 2       | 83    | 0     | 0.23   |
| Samsung   | MZVLB512HBJQ-000L7 | 512 GB | 2       | 79    | 0     | 0.22   |
| Samsung   | SSD 970 EVO Plus   | 250 GB | 5       | 67    | 0     | 0.19   |
| Samsung   | SSD 970 EVO Plus   | 500 GB | 2       | 51    | 0     | 0.14   |
| Samsung   | SSD 970 EVO        | 500 GB | 1       | 42    | 0     | 0.12   |
| Samsung   | PM981 NVMe         | 256 GB | 1       | 40    | 0     | 0.11   |
| Samsung   | SSD 960 EVO        | 1 TB   | 1       | 154   | 3     | 0.11   |
| Samsung   | MZVLB512HAJQ-00000 | 512 GB | 1       | 18    | 0     | 0.05   |
| Samsung   | MZVLB256HBHQ-000L7 | 256 GB | 1       | 17    | 0     | 0.05   |
| Samsung   | SSD 970 EVO Plus   | 1 TB   | 1       | 13    | 0     | 0.04   |
| Samsung   | MZVLQ256HAJD-00000 | 256 GB | 1       | 5     | 0     | 0.02   |
| Samsung   | MZVLW512HMJP-00000 | 512 GB | 1       | 5     | 0     | 0.02   |
