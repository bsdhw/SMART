WDC NVMe Drives
===============

This is a list of all tested WDC NVMe drive models and their MTBFs. See more
info on reliability test in the [README](https://github.com/bsdhw/SMART).

NVME by Model
------------

Please take all columns into account when reading the table. Pay attention on the
number of tested samples and power-on days. Simultaneous high values of both MTBF
and errors are possible if only rare drives in the subset encounter errors.

Days   — avg. days per sample,
Err    — avg. errors per sample,
MTBF   — avg. MTBF in years per sample.

| MFG       | Model              | Size   | Samples | Days  | Err   | MTBF   |
|-----------|--------------------|--------|---------|-------|-------|--------|
| WDC       | WDS100T3X0C-00SJG0 | 1 TB   | 1       | 456   | 0     | 1.25   |
| WDC       | WDS500G1B0C-00S6U0 | 500 GB | 1       | 257   | 0     | 0.71   |
| WDC       | WDS250G2B0C-00PXH0 | 250 GB | 1       | 137   | 0     | 0.38   |
| WDC       | PC SN730 SDBQNT... | 512 GB | 1       | 32    | 0     | 0.09   |
| WDC       | PC SN730 SDBQNT... | 1 TB   | 1       | 32    | 0     | 0.09   |
| WDC       | PC SN730 SDBQNT... | 256 GB | 1       | 23    | 0     | 0.06   |
| WDC       | PC SN520 SDAPNU... | 512 GB | 1       | 5     | 0     | 0.01   |
